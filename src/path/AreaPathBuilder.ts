import { LinePathBuilder } from './LinePathBuilder';

export class AreaPathBuilder extends LinePathBuilder {
	public beginPath(): void {
		this.path.moveTo(0, 0);
	}

	public endPath(xL: number): Path2D {
		this.path.lineTo(xL, 0);
		return this.path;
	}
}
