import { PointWithDataSet, Tooltip } from './Tooltip';

import { CanvasName } from '../core/CanvasManager';
import { Chart } from '../core/Chart';
import { ChartState } from '../core/ChartState';
import { EventName } from '../events/EventName';
import {
	mouseEventByName,
	normalizeEvent,
	subscribeToTouchEvents,
	DomEventByName, DomInteraction, MOUSE_MOVE_THROTTLE,
} from '../utils/DomUtils';
import { mouseEventToData } from '../utils/ProjectionUtils';
import { round, throttle } from '../utils/Utils';

export class TooltipInteraction implements DomInteraction {
	public eventByName: DomEventByName = mouseEventByName;
	private lastXPos: number | undefined;
	private readonly overlay: HTMLCanvasElement;
	private readonly onMouseMove: (event: Event) => void = throttle(event => {
		const normalized = normalizeEvent(event, this.state.chartRect);
		const [x] = mouseEventToData(normalized, this.state);
		if (!this.state.dataBounds.containsPointHorizontal(x)) {
			return;
		}
		// x = floorToPrecision(x, this.state.initialBounds.deltaX);
		if (x === this.lastXPos) {
			return;
		}
		this.lastXPos = x;
		const pointsWithDataSet = this.getDataByX(x);
		this.tooltip.setDataAndShow(pointsWithDataSet);
	}, MOUSE_MOVE_THROTTLE);

	constructor(
		private readonly chart: Chart,
		private readonly state: ChartState,
		private readonly tooltip: Tooltip,
	) {
		this.overlay = state.canvasManager.get(CanvasName.overlay) as HTMLCanvasElement;
		this.state.config.container.addEventListener(EventName.disableChart, () => {
			this.removeListeners(this.eventByName);
		});
		this.state.config.container.addEventListener(EventName.enableChart, () => {
			this.addListeners(this.eventByName);
		});
		subscribeToTouchEvents(this, this.overlay);
	}

	public addListeners(eventByName: DomEventByName): void {
		const passive = { passive: true };
		this.overlay.addEventListener(eventByName.leave, this.onMouseLeave, passive);
		this.overlay.addEventListener(eventByName.move, this.onMouseMove, passive);
		this.overlay.addEventListener(eventByName.down, this.onPointerDown, passive);
	}

	public removeListeners(eventByName: DomEventByName): void {
		this.overlay.removeEventListener(eventByName.leave, this.onMouseLeave);
		this.overlay.removeEventListener(eventByName.move, this.onMouseMove);
		this.overlay.removeEventListener(eventByName.down, this.onPointerDown);
	}

	public readonly onPointerDown = (event: Event): void => {
		this.onMouseMove(event);
	};

	private readonly onMouseLeave = (): void => {
		this.lastXPos = undefined;
		this.tooltip.hide();
	};

	private getDataByX(x: number): PointWithDataSet[] {
		// we believe that all dataSets have same X data index
		const firstVisible = this.chart.dataSets.find(ds => ds.isVisible);
		if (!firstVisible) {
			return [];
		}
		// const i = findNearestPointIndexByX(firstVisible.dataReader, x);
		const i = round(x);
		return this.chart.dataSets.reduce((results: PointWithDataSet[], dataSet) => {
			if (dataSet.isVisible) {
				results.push({
					dataSet,
					data: dataSet.dataReader.get(i),
					x: i,
				});
			}
			return results;
		}, []);
	}
}
