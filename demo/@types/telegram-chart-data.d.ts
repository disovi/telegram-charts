import { DataSetType } from '../../src';

export type TelegramColumn = [string, ...number[]];

export interface TelegramChartData {
	colors: Record<string, string>;
	names: Record<string, string>;
	types: Record<string, DataSetType>;
	columns: TelegramColumn[];
	y_scaled?: boolean;
	stacked?: boolean;
	percentage?: boolean;
}
