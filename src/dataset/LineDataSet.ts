import { BaseDataSet, BaseDataSetConfig, DataSetAttributes, LineStyle } from './BaseDataSet';
import { DataSetType } from './DataSetType';

import { ChartState } from '../core/ChartState';
import { applyLineStyle } from '../utils/CanvasUtils';

const DEFAULTS = {
	lineWidth: 1,
	color: 'blue',
	opacity: 1,
};

export interface LineDataSetConfig extends BaseDataSetConfig, LineStyle {
}

export class LineDataSet extends BaseDataSet<LineDataSetConfig> {
	public readonly lineStyle: LineStyle = {
		lineWidth: this.getOption('lineWidth'),
		color: this.getOption('color'),
		opacity: this.getOption('opacity'),
	};
	constructor(
		dataSet: LineDataSetConfig,
		attributes: DataSetAttributes,
		type: DataSetType = DataSetType.LINE,
	) {
		super(type, dataSet, DEFAULTS, attributes);
	}

	protected drawHelper(
		ctx: CanvasRenderingContext2D,
		state: ChartState,
		transform: DOMMatrix,
	): void {
		applyLineStyle(ctx, this.lineStyle, true);
		if (state.isMaster) {
			ctx.lineWidth = 1;
		}
		// we have to create path because we don't want line width to be scaled
		const path = new Path2D();
		path.addPath(this.path, transform);
		ctx.stroke(path);
	}
}
