export type EasingFunction = (x: number) => number;

export const enum EasingEffect {
	linear = 'linear',
	quadraticIn = 'quadraticIn',
	quadraticOut = 'quadraticOut',
	cubicIn = 'cubicIn',
	cubicOut = 'cubicOut',
}

// more http://robertpenner.com/easing/
export const easingFunctions: Record<EasingEffect, EasingFunction> = {
	linear: x => x,
	quadraticIn: x => x * x,
	quadraticOut: x => x * (2 - x),
	cubicIn: x => x * x * x,
	cubicOut: x => (--x) * x * x + 1,
};
