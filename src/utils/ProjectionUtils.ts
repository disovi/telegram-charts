import { Bounds } from './Bounds';
import { NormalizedEvent } from './DomUtils';
import { round } from './Utils';

import { ChartState } from '../core/ChartState';
import { PointTuple } from '../dataset/BaseDataSet';

export function mouseEventToChartCanvas(event: NormalizedEvent, state: ChartState): PointTuple {
	return [
		event.offsetX - state.chartBounds.innerBounds.left,
		event.offsetY - state.chartBounds.innerBounds.top,
	];
}

export function xDataToCanvas(x: number, state: { dataBounds: Bounds, scaleX: number}): number {
	return round((x - state.dataBounds.left) * state.scaleX);
}

export function yDataToCanvas(y: number, state: { dataBounds: Bounds, scaleY: number}): number {
	return round((state.dataBounds.top - y) * state.scaleY);
}

export function xCanvasToData(x: number, state: ChartState): number {
	return state.dataBounds.left + x / state.scaleX;
}

export function yCanvasToData(y: number, state: ChartState): number {
	return state.dataBounds.bottom - y / state.scaleY;
}

export function canvasToData([x, y]: PointTuple, state: ChartState): PointTuple {
	return [
		xCanvasToData(x, state),
		yCanvasToData(y, state),
	];
}

// virtual x starts from 0 and is scaled by deltaX
export function xVirtualToReal(x: number, initialBounds: { deltaX: number, xFrom: number }): number {
	return initialBounds.deltaX * x + initialBounds.xFrom;
}

export function mouseEventToData(event: NormalizedEvent, state: ChartState): PointTuple {
	return canvasToData(mouseEventToChartCanvas(event, state), state);
}
