import { AreaPathBuilder } from './AreaPathBuilder';
import { BarPathBuilder } from './BarPathBuilder';
import { BasePathBuilder, LinePathBuilder } from './LinePathBuilder';

import { DataSetType } from '../dataset/DataSetType';

export function createPathBuilder(type: DataSetType): BasePathBuilder {
	switch (type) {
		case DataSetType.LINE:
			return new LinePathBuilder();
		case DataSetType.AREA:
			return new AreaPathBuilder();
		case DataSetType.BAR:
			return new BarPathBuilder();
	}
}
