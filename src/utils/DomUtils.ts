export interface NormalizedEvent {
	offsetX: number;
	offsetY: number;
	clientX: number;
}

const enum DomEvent {
	MOVE = 'move',
	DOWN = 'down',
	UP = 'up',
	LEAVE = 'leave',
}

export type DomEventByName = Record<DomEvent, string>;

export interface DomInteraction {
	eventByName: DomEventByName;
	addListeners(eventByName: DomEventByName): void;
	removeListeners(eventByName: DomEventByName): void;
	onPointerDown?(event: Event): void;
}

export const touchEventByName: Record<DomEvent, string> = {
	[DomEvent.MOVE]: 'touchmove',
	[DomEvent.DOWN]: 'touchstart',
	[DomEvent.UP]: 'touchend',
	[DomEvent.LEAVE]: 'touchend',
};

export const mouseEventByName: Record<DomEvent, string> = {
	[DomEvent.MOVE]: 'mousemove',
	[DomEvent.DOWN]: 'mousedown',
	[DomEvent.UP]: 'mouseup',
	[DomEvent.LEAVE]: 'mouseleave',
};

export function subscribeToTouchEvents(domInteraction: DomInteraction, downTarget: HTMLElement): void {
	domInteraction.addListeners(mouseEventByName);
	window.addEventListener('touchstart', (event) => {
		domInteraction.removeListeners(mouseEventByName);
		domInteraction.eventByName = touchEventByName;
		domInteraction.addListeners(touchEventByName);
		if (domInteraction.onPointerDown && downTarget.contains(event.target as HTMLElement)) {
			domInteraction.onPointerDown(event);
		}
	}, { passive: true, once: true });
}

export const MOUSE_MOVE_THROTTLE = 1000 / 100;

export function createElementFromString<T extends HTMLElement>(html: string): T {
	const template = document.createElement('template');
	template.innerHTML = html.trim();
	return template.content.firstChild as T;
}

export function normalizeEvent(event: Event, rect: ClientRect): NormalizedEvent {
	const touchEvent = event as TouchEvent;
	if (touchEvent.targetTouches) {
		const touch = touchEvent.targetTouches[0];
		return Object.assign(touch, {
			offsetX: touchEvent.targetTouches[0].clientX - rect.left,
			offsetY: touchEvent.targetTouches[0].clientY - rect.top,
		});
	}
	return event as MouseEvent;
}
