import { createPathBuilder } from './pathBuilderFactory';

import { AnimationState } from '../animation/AnimationState';
import { ChartOptions } from '../core/ChartConfig';
import { DataSet } from '../dataset/dataSetFactory';
import { VerticalBounds } from '../utils/Bounds';
import { ScaleRange } from '../utils/ScaleRange';
import { round } from '../utils/Utils';

export class PathGenerator {
	private readonly ySum: number[];
	private readonly emptyState: ReadonlyArray<number>;
	private currentState: number[] = [];

	constructor(
		private readonly dataSets: DataSet[],
		private readonly size: number,
		private readonly options: ChartOptions,
	) {
		this.ySum = Array(size).fill(0);
		this.emptyState = this.ySum.slice();
	}

	public getVBoundsTo(scaleRange: ScaleRange): VerticalBounds | undefined {
		let stateTo;
		for (let i = this.dataSets.length - 1; i >= 0; i--) {
			const ds = this.dataSets[i];
			if (ds.shouldBeVisible) {
				stateTo = ds.stateTo;
				break;
			}
		}
		if (stateTo) {
			return this.getVBounds(scaleRange, stateTo);
		}
	}

	public getVBounds(scaleRange: ScaleRange, state: ReadonlyArray<number> = this.currentState): VerticalBounds {
		const vBounds = {
			top: 0,
			bottom: 0,
		};
		for (let i = scaleRange.indexBounds.left; i < scaleRange.indexBounds.right; i++) {
			vBounds.top = Math.max(vBounds.top, state[i]);
		}
		return vBounds;
	}

	public animatePaths(animation: AnimationState): void {
		const dataSets = this.dataSets.filter(ds => ds.shouldBeVisible || ds.isVisible);
		const pathBuilders = dataSets.map(ds => createPathBuilder(ds.type));
		for (let x = 0; x < this.size; x++) {
			this.currentState[x] = 0;
			for (let i = 0; i < dataSets.length; i++) {
				const ds = dataSets[i];
				const builder = pathBuilders[i];
				const y = animation.isFinished ? ds.state[x] : round(animation.applyToNumber((ds.stateFrom as number[])[x], (ds.stateTo as number[])[x]));
				if (x === 0) {
					builder.beginPath(y);
				}
				ds.state[x] = y;
				builder.addPoint(x, y);
				if (x === this.size - 1) {
					ds.setPath(builder.endPath(x, y), ds.state);
				}
				this.currentState[x] = Math.max(this.currentState[x], y);
			}
		}
	}

	public createPaths(createStateTo: boolean = false): void {
		const dataSets = this.dataSets.filter(ds => ds.shouldBeVisible);
		if (this.options.percentage) {
			this.calcSum(dataSets);
		}
		const states: number[][] = [];
		const pathBuilders = dataSets.map(ds => {
			states.push([]);
			return createPathBuilder(ds.type);
		});
		for (let x = 0; x < this.size && dataSets.length !== 0; x++) {
			let y = 0;
			const multiplier = this.options.percentage ? 100 / this.ySum[x] : 1;
			for (let i = 0; i < dataSets.length; i++) {
				const state = states[i];
				const ds = dataSets[i];
				const builder = pathBuilders[i];
				const yV = ds.dataReader.getY(x);
				if (this.options.stacked) {
					if (this.options.percentage && i === dataSets.length - 1) {
						y = 100;
					} else {
						y += yV * multiplier;
					}
				} else {
					y = yV;
				}
				y = round(y);
				state.push(y);
				if (!createStateTo) {
					if (x === 0) {
						builder.beginPath(y);
					}
					builder.addPoint(x, y);
				}
				if (x === this.size - 1) {
					if (createStateTo) {
						ds.setAnimationStateTo(state);
					} else {
						ds.setPath(builder.endPath(x, y), state);
					}
				}
			}
		}
		if (createStateTo) {
			return;
		}
		if (states.length) {
			this.currentState = states[states.length - 1].slice();
		}
	}

	public prepareAnimation(): void {
		this.createPaths(true);
		// update states for showing/hiding datasets
		let lastVisibleDs: DataSet | undefined;
		this.dataSets.forEach(ds => {
			if (ds.shouldBeVisible) {
				if (!ds.isVisible) {
					ds.stateFrom = lastVisibleDs ? lastVisibleDs.stateFrom : this.emptyState;
				} else {
					lastVisibleDs = ds;
				}
			} else if (ds.isVisible) {
				ds.setAnimationStateTo(lastVisibleDs ? lastVisibleDs.stateTo : this.emptyState);
			}
		});
	}

	private calcSum(dataSets: DataSet[]): void {
		for (let i = 0; i < this.size; i++) {
			this.ySum[i] = dataSets.reduce((r, ds) => r + ds.dataReader.getY(i), 0);
		}
	}
}
