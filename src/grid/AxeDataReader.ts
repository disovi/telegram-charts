import { AxeDirection, ChartAxeOptions } from './GridLinesAndLabels';

import { chartAnimator } from '../animation/chartAnimator';
import { AnimationState } from '../animation/AnimationState';
import { ChartState } from '../core/ChartState';
import { PointTuple } from '../dataset/BaseDataSet';
import { BoundingBox, Bounds, HorizontalBounds, VerticalBounds } from '../utils/Bounds';

export abstract class AxeDataReader<T extends HorizontalBounds | VerticalBounds = BoundingBox> {
	public dirtyLabels: boolean = true;
	public dataPerTick: number = 0;
	public animation?: AnimationState;
	protected labelsCount: number = 0;
	protected oldDataPerTick: number = 0;
	protected constructor(
		protected readonly state: ChartState,
		public readonly direction: AxeDirection,
		private readonly ctx: CanvasRenderingContext2D,
	) {
	}

	public abstract get options(): ChartAxeOptions;

	public abstract clear(ctx: CanvasRenderingContext2D): void;

	public animateToBounds(bounds: T, onFinish?: () => void): void {
		// if (this.animation) {
		// 	console.log('skip animation: ', bounds);
		// 	return;
		// }
		// console.log('animate to bounds: ', bounds);
		const dataPerTick = this.calcDataPerTick(bounds);
		// if (this.dataPerTick === dataPerTick) {
		// 	return;
		// }
		this.animate(dataPerTick, onFinish);
	}

	public abstract calcDataPerTick(dataBounds: T): number;

	public abstract getLinePoints(value: number): [PointTuple, PointTuple];

	public updateDataPerTick(): void {
		if (this.animation) {
			return;
		}
		const dataPerTick = this.calcDataPerTick(this.state.dataBounds as unknown as T);
		// if (Math.abs(this.dataPerTick - dataPerTick) > 0.05 * this.getDimension(this.state.dataBounds)) {
		if (this.dataPerTick !== dataPerTick) {
			this.animate(dataPerTick);
		}
	}

	public abstract getValueFrom(dataPerTick: number): number;
	public abstract getValueTo(dataPerTick: number): number;
	public abstract getMaxPossibleLabels(): number;

	protected abstract getDimension(bounds: Bounds): number;

	private animate(dataPerTick: number, onFinish?: () => void): void {
		this.oldDataPerTick = this.dataPerTick;
		const duration = this.oldDataPerTick ? this.state.config.options.animation.duration.gridAndLabels : 0;
		this.dataPerTick = dataPerTick;
		this.animation = new AnimationState(this.ctx, this, duration, {
			from: this.oldDataPerTick,
			to: this.dataPerTick,
			onFinish: () => {
				this.animation = undefined;
				if (onFinish) {
					onFinish();
				}
			},
		});
		chartAnimator.add(this.animation);
	}
}
