import { floorToPrecision, round } from '../utils/Utils';

const LOCALES = ['en-GB', 'en-US', 'ru-RU'];

export const enum DateAndNumberFormat {
	DATE_DAY_MONTH = 'dateDayMonth',
	DATE_FULL = 'dateFull',
	NUMBER_SCIENTIFIC = 'numberScientific',
	NUMBER_LOCAL = 'numberLocal',
}

type FormatNumber = (x: number) => string;

const dateDayMonthFormatter = new Intl.DateTimeFormat(LOCALES, {
	day: 'numeric',
	month: 'short',
});

const dateFullFormatter = new Intl.DateTimeFormat(LOCALES, {
	day: '2-digit',
	month: 'short',
	year: 'numeric',
	weekday: 'short',
});

const numberLocalFormatter = new Intl.NumberFormat(['ru-RU'], { useGrouping: true });

export const formatByName: Record<DateAndNumberFormat, FormatNumber> = {
	dateDayMonth: y => dateDayMonthFormatter.format(y),
	dateFull: y => dateFullFormatter.format(y),
	numberScientific: y => formatScientific(y),
	numberLocal: y => numberLocalFormatter.format(y),
};

const labelByPowOf10: Record<number, string> = {
	3: 'K',
	6: 'M',
	9: 'G',
	12: 'T',
};

function formatScientific(y: number): string {
	const powOf10 = floorToPrecision(Math.log10(y), 3);
	const precisionLabel = labelByPowOf10[powOf10];
	if (precisionLabel) {
		y = round(y / Math.pow(10, powOf10 - 2)) / 100;
		return `${y}${precisionLabel}`;
	}
	return `${y}`;
}
