import { BaseDataSet, BaseDataSetConfig, DataSetAttributes } from './BaseDataSet';
import { DataSetType } from './DataSetType';

import { ChartState } from '../core/ChartState';
import { applyFillStyle } from '../utils/CanvasUtils';

const DEFAULTS = {
};

export class BarDataSet extends BaseDataSet {
	constructor(options: BaseDataSetConfig, attributes: DataSetAttributes) {
		super(DataSetType.BAR, options, DEFAULTS, attributes);
	}

	protected drawHelper(
		ctx: CanvasRenderingContext2D,
		state: ChartState,
		transform: DOMMatrix,
	): void {
		applyFillStyle(ctx, this.options, true);
		ctx.transform(transform.a, transform.b, transform.c, transform.d, transform.e, transform.f);
		ctx.fill(this.path);
	}
}
