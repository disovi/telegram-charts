import { DataSetAttributes } from './BaseDataSet';
import { DataSetType } from './DataSetType';
import { LineDataSet, LineDataSetConfig } from './LineDataSet';

import { ChartState } from '../core/ChartState';
import { applyFillStyle } from '../utils/CanvasUtils';

export class AreaDataSet extends LineDataSet {
	constructor(dataSet: LineDataSetConfig, attributes: DataSetAttributes) {
		super(dataSet, attributes, DataSetType.AREA);
	}

	protected drawHelper(ctx: CanvasRenderingContext2D, state: ChartState, transform: DOMMatrix): void {
		applyFillStyle(ctx, this.options);
		ctx.transform(transform.a, transform.b, transform.c, transform.d, transform.e, transform.f);
		ctx.fill(this.path);
	}
}
