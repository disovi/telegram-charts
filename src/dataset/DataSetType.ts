export enum DataSetType {
	LINE = 'line',
	BAR = 'bar',
	AREA = 'area',
}
