import { tgDataToChartConfig } from './TgUtils';

import { debounce, getTheme, Chart, ThemeName } from '../../src';
import { TelegramChartData } from '../@types/telegram-chart-data';

const chartTemplate = document.getElementById('chart-wrapper') as HTMLTemplateElement;
const charts: Chart[] = [];
const THEME_KEY = 'THEME';
const CHARTS_COUNT = 5;
const selectedTheme = localStorage.getItem(THEME_KEY) || ThemeName.DAY;

async function initCharts(): Promise<void> {
	for (let i = 0; i < CHARTS_COUNT; i++) {
		const chartData = await fetch(`./data/${i + 1}/overview.json`).then(r => r.json());
		const container = document.getElementById('container') as HTMLDivElement;
		container.appendChild(chartTemplate.content.cloneNode(true));
		const chartDom = container.lastElementChild as HTMLDivElement;
		const chartConfig = tgDataToChartConfig(
			chartData as unknown as TelegramChartData,
			chartDom.querySelector<HTMLDivElement>('.t-legend')!,
		);
		if (selectedTheme) {
			const themeName = selectedTheme as ThemeName;
			chartConfig.options.theme = themeName;
			applyThemeToDom(themeName);
		}
		const chart = new Chart(
			chartDom.querySelector('.t-chart') as HTMLCanvasElement,
			chartDom.querySelector('.t-chart-master>.t-chart') as HTMLCanvasElement,
			chartConfig,
		);
		charts.push(chart);
	}
}

const switchTextByName: Record<ThemeName, string> = {
	night: 'Day Mode',
	day: 'Night Mode',
};

const NIGHT_THEME_CLASS = 't-night-theme';

const switchThemeLink = document.getElementById('switch-theme') as HTMLElement;
switchThemeLink.onclick = () => {
	const themeName = document.body.classList.contains(NIGHT_THEME_CLASS) ? ThemeName.DAY : ThemeName.NIGHT;
	applyThemeToDom(themeName);
	charts.forEach(c => c.applyTheme(getTheme(themeName)));
};

function applyThemeToDom(themeName: ThemeName): void {
	switchThemeLink.innerText = `Switch to ${switchTextByName[themeName]}`;
	if (themeName === ThemeName.NIGHT) {
		document.body.classList.add(NIGHT_THEME_CLASS);
	} else {
		document.body.classList.remove(NIGHT_THEME_CLASS);
	}
	localStorage.setItem(THEME_KEY, themeName);
}

const resizeCharts = () => {
	charts.forEach(chart => chart.resize());
};

window.addEventListener('resize', debounce(resizeCharts, 300), { passive: true });

initCharts().then(() => {
	// resizeCharts();
});
