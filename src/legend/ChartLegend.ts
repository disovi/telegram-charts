import { Chart } from '../core/Chart';
import { DataSet } from '../dataset/dataSetFactory';
import {
	createElementFromString,
	mouseEventByName,
	subscribeToTouchEvents,
	DomEventByName,
	DomInteraction,
} from '../utils/DomUtils';

export interface LegendOptions {
	container?: HTMLElement;
}

const LONG_PRESS_DURATION = 1000;
const ACTIVE_CLASS = 'active';

export class ChartLegend implements DomInteraction {
	public eventByName: DomEventByName = mouseEventByName;
	private readonly checkboxByDataSet: Map<DataSet, HTMLElement> = new Map();
	private readonly dataSetByCheckBox: WeakMap<HTMLElement, DataSet> = new WeakMap();
	private longPressTimeout: number = 0;
	constructor(
		private readonly container: HTMLElement,
		// tslint:disable-next-line: no-unused-variable
		private readonly options: LegendOptions,
		private readonly chart: Chart,
	) {
		subscribeToTouchEvents(this, this.container);
	}

	public addListeners(eventByName: DomEventByName): void {
		this.container.addEventListener(eventByName.down, this.onPointerDown, { passive: true });
	}

	public removeListeners(eventByName: DomEventByName): void {
		this.container.removeEventListener(eventByName.down, this.onPointerDown);
	}

	public create(): void {
		this.chart.dataSets.forEach(dataSet => {
			const button = this.genCheckbox(dataSet);
			this.container.append(button);
		});
	}

	public readonly onPointerDown = (e: Event): void => {
		clearTimeout(this.longPressTimeout);
		this.longPressTimeout = 0;
		const mouseEvent = e as MouseEvent;
		if (mouseEvent.button && mouseEvent.button !== 0) {
			return;
		}
		let target = e.target as HTMLElement;
		let dataSet = this.dataSetByCheckBox.get(target);
		if (!dataSet) {
			target = target.parentElement as HTMLElement;
			dataSet = this.dataSetByCheckBox.get(target);
			if (!dataSet) {
				return;
			}
		}
		target.addEventListener(this.eventByName.up, this.onUpListener, {
			once: true, passive: true,
		});
		target.addEventListener(this.eventByName.leave, () => {
			clearTimeout(this.longPressTimeout);
			target.removeEventListener(this.eventByName.up, this.onUpListener);
		},{ once: true, passive: true });
		this.longPressTimeout = window.setTimeout(() => {
			this.showSelectedDataSet(dataSet as DataSet);
			this.longPressTimeout = 0;
		}, LONG_PRESS_DURATION);
	};

	private genCheckbox(dataSet: DataSet): HTMLElement {
		const checkbox = createElementFromString<HTMLDivElement>(
			`<button class="t-checkbox"><label>${dataSet.name}</label></button>`,
		);
		checkbox.classList.add(ACTIVE_CLASS);
		checkbox.style.backgroundColor = dataSet.options.color;
		checkbox.style.color = dataSet.options.color;
		checkbox.style.borderColor = dataSet.options.color;
		this.dataSetByCheckBox.set(checkbox, dataSet);
		this.checkboxByDataSet.set(dataSet, checkbox);
		return checkbox;
	}

	private readonly onUpListener = (e: Event): void => {
		if (!this.longPressTimeout) {
			return;
		}
		clearTimeout(this.longPressTimeout);
		const dataSet = this.dataSetByCheckBox.get(e.currentTarget as HTMLElement);
		if (!dataSet) {
			return;
		}
		// this.showSelectedDataSet(dataSet);
		dataSet.shouldBeVisible = !dataSet.shouldBeVisible;
		this.setCheckboxValue(dataSet, dataSet.shouldBeVisible);
		this.chart.updateDataSetsVisibility();
	};

	private showSelectedDataSet(dataSet: DataSet): void {
		this.chart.dataSets.forEach(ds => {
			ds.shouldBeVisible = ds === dataSet;
			this.setCheckboxValue(ds, ds.shouldBeVisible);
		});
		this.chart.updateDataSetsVisibility();
	}

	private setCheckboxValue(dataSet: DataSet, checked: boolean): void {
		const checkbox = this.checkboxByDataSet.get(dataSet);
		if (!checkbox) {
			return;
		}
		if (checked) {
			checkbox.classList.add(ACTIVE_CLASS);
		} else {
			checkbox.classList.remove(ACTIVE_CLASS);
		}
	}
}
