import { AreaDataSet } from './AreaDataSet';
import { BarDataSet } from './BarDataSet';
import { BaseDataSetConfig, DataSetAttributes } from './BaseDataSet';
import { DataSetType } from './DataSetType';
import { LineDataSet, LineDataSetConfig } from './LineDataSet';

export type DataSetConfig = LineDataSetConfig | BaseDataSetConfig;
export type DataSet = LineDataSet | BarDataSet | AreaDataSet;

export function createDataSet(config: DataSetConfig, attributes: DataSetAttributes): DataSet {
	switch (config.type) {
		case DataSetType.BAR:
			return new BarDataSet(config, attributes);
		case DataSetType.AREA:
			return new AreaDataSet(config as LineDataSetConfig, attributes);
		case DataSetType.LINE:
		default:
			return new LineDataSet(config as LineDataSetConfig, attributes);
	}
}
