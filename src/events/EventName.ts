import { BoundingBox } from '../utils/Bounds';

export enum EventName {
	disableChart = 'disable-chart',
	enableChart = 'enable-chart',
	updateVBounds = 'update-vbounds',
}

declare global {
	interface HTMLElementEventMap {
		[EventName.disableChart]: CustomEvent;
		[EventName.enableChart]: CustomEvent;
		[EventName.updateVBounds]: CustomEvent<{ from: BoundingBox, to: BoundingBox}>;
	}
}
