import { drawHandle } from './CurtainUtils';

import { ChartState } from '../core/ChartState';
import { FillStyle } from '../dataset/BaseDataSet';
import { Bounds } from '../utils/Bounds';

export interface HandleOptions extends FillStyle {
	width: number;
	cornerRadius: number;
}

export class CurtainHandle {
	private readonly _bounds: Bounds;
	private readonly options: HandleOptions;

	constructor(
		private readonly state: ChartState,
		public readonly isLeft: boolean,
		leftOffset: number,
	) {
		this.options = this.state.config.options.masterChart.curtain.handle;
		this._bounds = new Bounds({
			top: 0,
			left: leftOffset,
			bottom: state.chartBounds.innerBounds.height,
			right: leftOffset + this.options.width,
		});
	}

	public get bounds(): Readonly<Bounds> {
		return this._bounds;
	}

	public containsPointHorizontal(x: number, r: number): boolean {
		return this._bounds.containsPointHorizontal(x, r);
	}

	public draw(ctx: CanvasRenderingContext2D): void {
		drawHandle(ctx, this.bounds.left, this.bounds.height, this.options, this.isLeft);
	}
}
