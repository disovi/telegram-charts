import { AxeDataReader } from './AxeDataReader';
import { AxeDirection, ChartAxeOptions } from './GridLinesAndLabels';
import { calcDataPerTick } from './TickUtils';

import { ChartState } from '../core/ChartState';
import { PointTuple } from '../dataset/BaseDataSet';
import { VerticalBounds } from '../utils/Bounds';
import { clearBounds } from '../utils/CanvasUtils';
import { yDataToCanvas } from '../utils/ProjectionUtils';
import { floorToPrecision } from '../utils/Utils';

export class YDataReader extends AxeDataReader<VerticalBounds> {
	constructor(
		state: ChartState,
		ctx: CanvasRenderingContext2D,
	) {
		super(state, AxeDirection.VERTICAL, ctx);
		this.labelsCount = Math.floor(this.state.chartBounds.innerBounds.height / this.options.ticks.minPointsPerTick);
	}

	public get options(): ChartAxeOptions {
		return this.state.config.options.yAxis;
	}

	public clear(ctx: CanvasRenderingContext2D): void {
		// we clear y axe on chart clear because they intersect
		const { chartBounds } = this.state;
		clearBounds(ctx, {
			left: 0,
			right: chartBounds.right,
			top: 0,
			bottom: chartBounds.innerBounds.bottom,
		}, 1);
	}

	public calcDataPerTick(dataBounds: VerticalBounds): number {
		return calcDataPerTick(
			this.options.ticks,this.state.chartBounds.innerBounds.height, this.getDimension(dataBounds), AxeDirection.VERTICAL,
		);
		// return round(dataBounds.height / (this.labelsCount - 1)); // 1.05 for padding from top
	}

	public getLinePoints(v: number): [PointTuple, PointTuple] {
		const y = yDataToCanvas(v, this.state);
		return [
			[this.options.ticks.labelOffset.x, y],
			[this.state.chartBounds.innerBounds.width, y],
		];
	}

	public getValueFrom(dataPerTick: number): number {
		const from = this.state.initialBounds.dataBounds.bottom;
		return from + floorToPrecision(this.state.dataBounds.bottom - from, dataPerTick);
	}

	public getValueTo(): number {
		return this.state.dataBounds.top;
	}

	public getMaxPossibleLabels(): number {
		return 8;
	}

	protected getDimension(bounds: VerticalBounds): number {
		return bounds.top - bounds.bottom;
	}
}
