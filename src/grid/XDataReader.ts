import { AxeDataReader } from './AxeDataReader';
import { AxeDirection, ChartAxeOptions } from './GridLinesAndLabels';
import { calcDataPerTick } from './TickUtils';

import { ChartState } from '../core/ChartState';
import { PointTuple } from '../dataset/BaseDataSet';
import { BoundingBox, HorizontalBounds } from '../utils/Bounds';
import { clearBounds } from '../utils/CanvasUtils';
import { xDataToCanvas } from '../utils/ProjectionUtils';
import { floorToPrecision } from '../utils/Utils';

export class XDataReader extends AxeDataReader<HorizontalBounds> {
	constructor(
		state: ChartState,
		ctx: CanvasRenderingContext2D,
	) {
		super(state, AxeDirection.HORIZONTAL, ctx);
		this.labelsCount = Math.floor(this.state.chartBounds.innerBounds.width / this.options.ticks.minPointsPerTick) + 1;
	}

	public get options(): ChartAxeOptions {
		return this.state.config.options.xAxis;
	}

	public clear(ctx: CanvasRenderingContext2D): void {
		const { chartBounds } = this.state;
		clearBounds(ctx, {
			left: 0,
			right: chartBounds.right,
			top: chartBounds.innerBounds.bottom,
			bottom: chartBounds.bottom,
		}, 0);
	}

	public calcDataPerTick(dataBounds: BoundingBox): number {
		return calcDataPerTick(
			this.options.ticks,
			this.state.chartBounds.innerBounds.width,
			this.getDimension(dataBounds),
			this.direction,
		);
	}

	public getLinePoints(v: number): [PointTuple, PointTuple] {
		const x = xDataToCanvas(v, this.state);
		return [
			[x, this.state.chartBounds.innerBounds.height],
			[x, 0],
		];
	}

	public getValueFrom(dataPerTick: number): number {
		const from = this.state.initialBounds.dataBounds.bottom;
		return from + floorToPrecision(this.state.dataBounds.left - from, dataPerTick);
	}

	public getValueTo(): number {
		return this.state.dataBounds.right;
	}

	public getMaxPossibleLabels(): number {
		return 10;
	}

	protected getDimension(bounds: BoundingBox): number {
		return bounds.right - bounds.left;
	}
}
