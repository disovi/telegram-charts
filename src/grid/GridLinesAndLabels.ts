import { AxeDataReader } from './AxeDataReader';
import { XDataReader } from './XDataReader';
import { YDataReader } from './YDataReader';

import { chartAnimator } from '../animation/chartAnimator';
import { AnimationState } from '../animation/AnimationState';
import { CanvasName } from '../core/CanvasManager';
import { Chart } from '../core/Chart';
import { FontStyle, Point } from '../core/ChartConfig';
import { ChartState } from '../core/ChartState';
import { LineStyle, PointTuple } from '../dataset/BaseDataSet';
import { EventName } from '../events/EventName';
import { BoundingBox, VerticalBounds } from '../utils/Bounds';
import { applyFontStyle, applyLineStyle, clearBounds } from '../utils/CanvasUtils';
import { xVirtualToReal } from '../utils/ProjectionUtils';
import { ceilToPrecision, noop, round } from '../utils/Utils';

export const enum AxeDirection {
	HORIZONTAL = 'horizontal',
	VERTICAL = 'vertical',
}

export interface ChartAxeOptions {
	gridLines: GridLinesOptions;
	ticks: TickOptions;
	min?: number;
}

export type FormatLabel = (v: number, state: ChartState) => string;

export interface TickOptions extends FontStyle {
	display: boolean;
	minPointsPerTick: number;
	minDeltaPerTick?: number;
	formatLabel: FormatLabel;
	labelOffset: Point;
	opacity: number;
}

export interface GridLinesOptions extends LineStyle {
	display: boolean;
}

function drawLabels(
	state: ChartState,
	ctx: CanvasRenderingContext2D,
	v: number, p1: PointTuple, p2: PointTuple,
	index: number,
	ticks: TickOptions,
): void {
	const label = ticks.formatLabel(v, state);
	ctx.fillText(label, p1[0], p1[1] + ticks.labelOffset.y);
}

function drawXLabels(
	state: ChartState,
	ctx: CanvasRenderingContext2D,
	v: number, p1: PointTuple, p2: PointTuple,
	index: number,
	ticks: TickOptions,
): void {
	const textAlign = ctx.textAlign;
	if (index === 0) {
		ctx.textAlign = 'left';
	}
	const label = ticks.formatLabel(xVirtualToReal(v, state.initialBounds), state);
	ctx.fillText(label, p1[0], p1[1] + ticks.labelOffset.y);
	ctx.textAlign = textAlign;
}

function drawLabelsYScaled(
	state: ChartState,
	ctx: CanvasRenderingContext2D,
	v: number, p1: PointTuple, p2: PointTuple,
	index: number,
	ticks: TickOptions,
	chart: Chart,
): void {
	ctx.save();
	chart.dataSets.forEach((ds, i) => {
		if (!ds.shouldBeVisible) {
			return;
		}
		// if (ds.animation) {
		// 	ctx.globalAlpha = ds.animation.applyToNumber(ds.animation.from, ds.animation.to);
		// }
		const label = ticks.formatLabel(round(v / ds.scaleY), state);
		let pos;
		if (i) {
			pos = p2;
			ctx.textAlign = 'right';
		} else {
			pos = p1;
			ctx.textAlign = 'left';
		}
		ctx.fillStyle = ds.options.color;
		ctx.fillText(label, pos[0], pos[1] + ticks.labelOffset.y);
	});
	ctx.restore();
}

export class GridLinesAndLabels {
	private readonly xDataReader: XDataReader;
	private readonly yDataReader: YDataReader;
	private readonly drawXLabels: typeof drawLabels = drawXLabels;
	private readonly drawYLabels: typeof drawLabelsYScaled = drawLabels;
	private readonly ctx: CanvasRenderingContext2D;
	private isDirty: boolean = true;

	private readonly readers: AxeDataReader[];
	constructor(
		protected readonly state: ChartState,
		protected readonly chart: Chart,
	) {
		this.ctx = this.state.canvasManager.create(
			CanvasName.gridAndLabels,
			{
				zIndex: 1,
			});
		chartAnimator.register(this.ctx, this.draw);
		if (this.state.config.options.yScaled) {
			this.drawYLabels = drawLabelsYScaled;
		}
		this.xDataReader = new XDataReader(this.state, this.ctx);
		this.yDataReader = new YDataReader(this.state, this.ctx);
		this.readers = [this.xDataReader, this.yDataReader];
		this.state.config.container.addEventListener(EventName.disableChart, () => {
			this.markDirty();
		});
		this.state.config.container.addEventListener(EventName.enableChart, () => {
			this.singleDraw();
		});
		this.state.config.container.addEventListener(EventName.updateVBounds, this.onBoundsChangeListener);
	}

	public updateDataBounds(oldBounds: BoundingBox, newBounds: VerticalBounds | BoundingBox, skipBoundsUpdate: boolean): void {
		if (oldBounds.left !== (newBounds as BoundingBox).left
			|| oldBounds.right !== (newBounds as BoundingBox).right) {
			this.xDataReader.dirtyLabels = true;
		}
		if (oldBounds.top !== newBounds.top || oldBounds.bottom !== newBounds.bottom) {
			this.yDataReader.dirtyLabels = true;
		}
		if (!(this.xDataReader.dirtyLabels || this.yDataReader.dirtyLabels)) {
			return;
		}
		if (this.yDataReader.dirtyLabels) {
			const dataPerTick = this.yDataReader.calcDataPerTick(newBounds);
			if (dataPerTick !== this.yDataReader.dataPerTick) {
				let onFinish;
				if (skipBoundsUpdate) {
					this.state.config.container.removeEventListener(EventName.updateVBounds, this.onBoundsChangeListener);
					onFinish = () => {
						this.state.config.container.addEventListener(EventName.updateVBounds, this.onBoundsChangeListener);
					};
				}
				this.yDataReader.animateToBounds(newBounds, onFinish);
				return;
			}
		}
		this.singleDraw();
	}

	public readonly draw = (animations: AnimationState[]): void => {
		const ctx = this.ctx;
		let bounds;
		if (this.isDirty || this.state.isDisabled) {
			bounds = this.state.chartBounds;
			this.isDirty = false;
			clearBounds(ctx, bounds, 1);
		}
		/*else {
			bounds = this.state.chartBounds.innerBounds.toBoundingBox();
			bounds.left = 0;
			if (this.state.options.options.yScaled) {
				bounds.right = this.state.chartBounds.right;
			}
		}*/
		if (this.state.isDisabled) {
			return;
		}
		// const dataBoundsAnimation = animations.find(a => a.target === this.state && !a.executed);
		// if (dataBoundsAnimation) {
		// 	this.yDataReader.animateToBounds(dataBoundsAnimation.to);
		// }
		this.readers.forEach((reader) => {
			const animation = animations.find(a => a.target === reader);
			this.drawLinesAndLabels(ctx, reader, animation);
		});
	};

	public markDirtyY(): void {
		this.yDataReader.dirtyLabels = true;
		// this.isDirty = true;
		this.singleDraw();
	}

	public markDirty(): void {
		// console.log('markDirty');
		this.readers.forEach(r => r.dirtyLabels = true);
		this.isDirty = true;
		this.singleDraw();
	}

	private drawLinesAndLabels(
		ctx: CanvasRenderingContext2D,
		axeReader: AxeDataReader,
		animation?: AnimationState | undefined,
	): void {
		if (axeReader.dirtyLabels && !animation) {
			axeReader.updateDataPerTick();
		}
		animation = animation || axeReader.animation;
		if (animation) {
			axeReader.dirtyLabels = true;
		}
		if (!axeReader.dirtyLabels) {
			return;
		}
		ctx.save();
		if (axeReader.direction === AxeDirection.VERTICAL) {
			ctx.beginPath();
			ctx.rect(0,0, this.state.chartBounds.width, this.state.chartBounds.innerBounds.height);
			ctx.clip();
		}
		axeReader.clear(ctx);
		ctx.translate(this.state.chartBounds.innerBounds.left, this.state.chartBounds.innerBounds.top);
		const { gridLines, ticks } = axeReader.options;
		applyLineStyle(ctx, gridLines);
		applyFontStyle(ctx, ticks);
		const shouldDrawLabels = ticks.display && axeReader.dirtyLabels;
		const _drawLabels = shouldDrawLabels
			? (axeReader.direction === AxeDirection.HORIZONTAL ? this.drawXLabels : this.drawYLabels)
			: noop;
		if (animation && !animation.isFinished) {
			const linesOpacity = animation.applyToNumber(0, axeReader.options.gridLines.opacity);
			const labelsOpacity = animation.applyToNumber(0, axeReader.options.ticks.opacity);
			this.drawLinesAndLabelsHelper(
				ctx, axeReader,
				linesOpacity, labelsOpacity,
				animation.to, _drawLabels,
			);
			if (animation.from) {
				this.drawLinesAndLabelsHelper(
					ctx, axeReader,
					axeReader.options.gridLines.opacity - linesOpacity,
					axeReader.options.ticks.opacity - labelsOpacity,
					animation.from, _drawLabels,
				);
			}
		} else {
			this.drawLinesAndLabelsHelper(
				ctx, axeReader,
				axeReader.options.gridLines.opacity,
				axeReader.options.ticks.opacity,
				axeReader.dataPerTick, _drawLabels,
			);
		}
		ctx.restore();
		axeReader.dirtyLabels = false;
	}

	private readonly onBoundsChangeListener = ({ detail: { from, to } }: CustomEvent<{ from: BoundingBox, to: BoundingBox }>) => {
		this.updateDataBounds(from, to, false);
	};

	private drawLinesAndLabelsHelper(
		ctx: CanvasRenderingContext2D,
		axeReader: AxeDataReader,
		lineOpacity: number,
		labelOpacity: number,
		dataPerTick: number,
		_drawLabels: typeof drawLabelsYScaled,
	): void {
		const { ticks, gridLines } = axeReader.options;
		if (gridLines.display) {
			ctx.beginPath();
		}
		let v = axeReader.getValueFrom(dataPerTick);
		const to = axeReader.getValueTo(dataPerTick);
		const count = (to - v) / dataPerTick;
		if (count > axeReader.getMaxPossibleLabels()) {
			dataPerTick = ceilToPrecision((to - v) / axeReader.getMaxPossibleLabels(), dataPerTick);
		}
		ctx.globalAlpha = labelOpacity;
		let i = 0;
		for (;v < to; v += dataPerTick) {
			const [p1, p2] = axeReader.getLinePoints(v);
			if (gridLines.display) {
				ctx.moveTo(p1[0], p1[1]);
				ctx.lineTo(p2[0], p2[1]);
			}
			_drawLabels(this.state, ctx, v, p1, p2, i++, ticks, this.chart);
		}
		if (gridLines.display) {
			ctx.globalAlpha = lineOpacity;
			ctx.stroke();
		}
	}

	private singleDraw(): void {
		chartAnimator.add(new AnimationState(this.ctx, this,0));
	}
}
