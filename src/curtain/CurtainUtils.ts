import { HandleOptions } from './CurtainHandle';

import { HorizontalBounds } from '../utils/Bounds';
import { applyFillStyle, createCanvasOffscreen, drawBoxShape } from '../utils/CanvasUtils';

export function getRestrictedMovementForBounds(
	bounds: HorizontalBounds, movementX: number, restriction: HorizontalBounds,
): number {
	let movementResult;
	if (
		(bounds.left === restriction.left && movementX < 0)
		|| (bounds.right === restriction.right && movementX > 0)
	) {
		return 0;
	}
	if (bounds.left + movementX < restriction.left) {
		movementResult = restriction.left - bounds.left;
	} else if (bounds.right + movementX > restriction.right) {
		movementResult = restriction.right - bounds.right;
	} else {
		movementResult = movementX;
	}
	return movementResult;
}

let _canvas: HTMLCanvasElement | undefined;
let handleColor = '';

export function drawHandle(
	ctx: CanvasRenderingContext2D,
	x: number,
	handleHeight: number,
	options: HandleOptions,
	isLeft: boolean,
): void {
	if (!_canvas) {
		_canvas = createCanvasOffscreen({ width: options.width * 2, height: handleHeight });
	}
	if (options.color !== handleColor) {
		drawHandlesOffscreen(_canvas, options, handleHeight);
	}
	const sWidth = _canvas.width / 2;
	const sHeight = _canvas.height;
	const sX = isLeft ? 0 : sWidth;
	// disable scale
	ctx.drawImage(_canvas, sX, 0, sWidth, sHeight, x, 0, sWidth / window.devicePixelRatio, sHeight / window.devicePixelRatio);
}

function drawHandlesOffscreen(canvas: HTMLCanvasElement, options: HandleOptions, handleHeight: number): void {
	const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	applyFillStyle(ctx, options);
	drawBoxShape(ctx, {
		left: 0,
		right: options.width * 2,
		top: 0,
		bottom: handleHeight,
	}, options.cornerRadius);
	handleColor = options.color;
	ctx.fill();
	const lineHeight = handleHeight / 5;
	const cX1 = options.width / 2;
	const cX2 = cX1 + options.width;
	const cY = (handleHeight - lineHeight) / 2;
	ctx.strokeStyle = '#ffffff';
	ctx.lineCap = 'round';
	ctx.lineWidth = 1;
	ctx.beginPath();
	ctx.moveTo(cX1, cY);
	ctx.lineTo(cX1, cY + lineHeight);
	ctx.moveTo(cX2, cY);
	ctx.lineTo(cX2, cY + lineHeight);
	ctx.stroke();
}
