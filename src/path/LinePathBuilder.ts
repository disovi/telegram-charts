export interface BasePathBuilder {
	beginPath(y0: number): void;
	endPath(xL: number, yL: number): Path2D;
	addPoint(x: number, y: number): void;
}

export class LinePathBuilder implements BasePathBuilder {
	protected path: Path2D = new Path2D();

	public addPoint(x: number, y: number): void {
		this.path.lineTo(x, y);
	}

	public beginPath(y0: number): void {
		this.path.moveTo(0, y0);
	}

	public endPath(xL: number): Path2D {
		return this.path;
	}
}
