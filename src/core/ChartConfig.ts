import { DeepPartial } from 'ts-essentials';

import { MasterChartOptions } from './MasterChart';

import { AnimationOptions } from '../animation/AnimationState';
import { BaseDataSetConfig } from '../dataset/BaseDataSet';
import { formatByName } from '../grid/Formatters';
import { ChartAxeOptions } from '../grid/GridLinesAndLabels';
import { LegendOptions } from '../legend/ChartLegend';
import { getTheme, ChartTheme, ThemeName } from '../theme/ChartTheme';
import { TooltipOptions } from '../tooltip/Tooltip';
import { BoundingBox } from '../utils/Bounds';
import { assignDeeply } from '../utils/Utils';

export interface Point {
	x: number;
	y: number;
}

export interface ChartConfig {
	data: ChartData;
	options: DeepPartial<ChartOptions>;
}

export interface ChartOptions {
	theme: ThemeName;
	masterChart: MasterChartOptions;
	xAxis: ChartAxeOptions;
	yAxis: ChartAxeOptions;
	legend?: LegendOptions;
	animation: AnimationOptions;
	tooltip: TooltipOptions;
	padding: BoundingBox; // BoundingBox are used to give some place for labels
	backgroundColor?: string;
	yScaled?: boolean;
	stacked?: boolean;
	percentage?: boolean;
	barChartOpacity?: number;
}

export interface FontStyle {
	fontSize: number;
	fontFamily: string;
	fontColor?: string;
	textAlign?: CanvasTextAlign;
	fontWeight?: string;
	fontStyle?: string;
	opacity?: number;
}

export interface ChartData {
	dataSets: BaseDataSetConfig[];
}

const DEFAULT_FONT_FAMILY = 'Lucida Console';

export class ChartConfigImpl implements ChartConfig {
	public readonly data!: ChartData;
	public readonly options: Required<ChartOptions> = {
		theme: ThemeName.DAY,
		backgroundColor: 'white',
		barChartOpacity: 0.6,
		yScaled: false,
		stacked: false,
		percentage: false,
		masterChart: {
			curtain: {
				minVisiblePoints: 10,
				handle:{
					opacity: 1,
					width: 10,
					color: '#C0D1E1',
					cornerRadius: 6,
				},
				coveredArea: {
					opacity: 0.6,
					color: '#E2EEF9',
				},
			},
		},
		legend: {
			container: undefined,
		},
		tooltip: {
			enabled: true,
			showVerticalLine: true,
			line: {
				lineWidth: 1,
				color: '#545454',
				opacity: 0.4,
			},
			boxShadowColor: '#545454',
			pointRadius: 5,
			tooltipCornerRadius: 5,
			distanceFromTop: 10,
			rowGap: 5,
			formatX: formatByName.dateFull,
			formatY: formatByName.numberLocal,
			headerStyle: {
				fontSize: 12,
				fontFamily: DEFAULT_FONT_FAMILY,
				fontColor: 'black',
				fontWeight: 'bold',
			},
			dataSetNameStyle: {
				fontSize: 12,
				fontFamily: DEFAULT_FONT_FAMILY,
				fontColor: 'black',
			},
			valueStyle: {
				fontSize: 12,
				fontFamily: DEFAULT_FONT_FAMILY,
				fontWeight: 'bold',
			},
			padding: {
				left: 10,
				right: 10,
				top: 10,
				bottom: 10,
			},
		},
		animation: {
			duration: {
				gridAndLabels: 400,
				dataSetShowHide: 300,
				scroll: 300,
			},
		},
		padding: {
			left: 5,
			right: 5,
			bottom: 30,
			top: 5,
		},
		xAxis: {
			gridLines: {
				display: false,
				lineWidth: 1,
				color: '#182D3B',
				opacity: 1,
			},
			ticks: {
				display: true,
				minPointsPerTick: 50,
				fontColor: '#182D3B',
				fontFamily: DEFAULT_FONT_FAMILY,
				fontSize: 10,
				formatLabel: formatByName.dateDayMonth,
				labelOffset: {
					x: 0,
					y: 20,
				},
				textAlign: 'center',
				opacity: 1,
			},
		},
		yAxis: {
			gridLines: {
				display: true,
				lineWidth: 1,
				color: '#182D3B',
				opacity: 1,
			},
			ticks: {
				display: true,
				minPointsPerTick: 50,
				fontColor: '#182D3B',
				fontFamily: DEFAULT_FONT_FAMILY,
				fontSize: 10,
				formatLabel: formatByName.numberScientific,
				labelOffset: {
					x: 5,
					y: -5,
				},
				textAlign: 'left',
				opacity: 1,
			},
		},
	};

	constructor(
		public readonly container: HTMLCanvasElement,
		config: ChartConfig,
	) {
		if (!container) {
			throw new Error('Container is required');
		}
		if (config.options.theme) {
			const theme = getTheme(config.options.theme);
			this.applyTheme(theme);
		}
		assignDeeply<ChartConfig>(this, config);
	}

	public getOrDefault<T>(v: T | undefined, def: T): T {
		return typeof v === 'undefined' ? def : v;
	}

	public applyTheme(theme: DeepPartial<ChartTheme>): void {
		assignDeeply(this.options, theme);
	}
}
