import { AbstractDataReader, DataReader } from './DataReader';

export class WrappedDataReader<T extends DataReader> extends AbstractDataReader {
	constructor(
		protected readonly dataReader: T,
	) {
		super();
	}

	public getY(index: number): number {
		return this.dataReader.getY(index);
	}

	public getX(index: number): number {
		return this.dataReader.getX(index);
	}

	public get size(): number {
		return this.dataReader.size;
	}
}
