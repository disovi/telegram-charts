import { PointWithDataSet, Tooltip, TooltipOptions } from './Tooltip';
import { TooltipInteraction } from './TooltipInteraction';

import { chartAnimator } from '../animation/chartAnimator';
import { AnimationState } from '../animation/AnimationState';
import { Chart } from '../core/Chart';
import { ChartState } from '../core/ChartState';
import { PointTuple } from '../dataset/BaseDataSet';
import { DataSetType } from '../dataset/DataSetType';
import { LineDataSet } from '../dataset/LineDataSet';
import { Dimensions } from '../utils/Bounds';
import { BoundsWithPadding } from '../utils/BoundsWithPadding';
import { applyFillStyle, applyFontStyle, applyLineStyle, drawBoxShape } from '../utils/CanvasUtils';
import { xDataToCanvas, yDataToCanvas } from '../utils/ProjectionUtils';
import { round } from '../utils/Utils';

interface TooltipBodyModel {
	rowHeight: number;
	width: number;
	percentageOffset: number;
}

export interface TooltipModel {
	header: Dimensions;
	body: TooltipBodyModel;
	width: number;
	boxPath: Path2D;
}

interface TooltipModelWithBB extends TooltipModel {
	bb: BoundsWithPadding;
}

const LINE_GAP = 10;
const BOX_BORDER = 2;

export class TooltipVertical implements Tooltip {
	private _isVisible: boolean = false;
	private pointsWithDataSets: PointWithDataSet[] | undefined;
	// tslint:disable-next-line: no-unused-variable
	private readonly tooltipInteraction: TooltipInteraction;
	private readonly options: TooltipOptions;
	private readonly hasSummary: boolean;
	private model: TooltipModelWithBB | undefined;

	constructor(
		private readonly state: ChartState,
		chart: Chart,
	) {
		this.tooltipInteraction = new TooltipInteraction(chart, state, this);
		this.options = state.config.options.tooltip;
		this.hasSummary = Boolean(this.state.config.options.stacked && !this.state.config.options.percentage);
	}

	public get ctx(): CanvasRenderingContext2D {
		return this.state.overlayCtx;
	}

	public get isVisible(): boolean {
		return this._isVisible;
	}

	public hide(): void {
		this.setDataAndShow(undefined);
	}

	public setDataAndShow(pointsWithName: PointWithDataSet[] | undefined): void {
		this.pointsWithDataSets = pointsWithName;
		this._isVisible = Boolean(pointsWithName && pointsWithName.length);
		chartAnimator.add(new AnimationState(this.state.overlayCtx, this, 0));
	}

	public draw(): void {
		if (!this.options.enabled || !this.isVisible) {
			return;
		}
		if (!(this.pointsWithDataSets && this.pointsWithDataSets.length)) {
			return;
		}
		const { ctx } = this;
		ctx.save();
		// ctx.translate(this.state.chartBounds.innerBounds.left, this.state.chartBounds.innerBounds.top);
		const topY = this.findTopDataSetPointOnScreen(this.pointsWithDataSets);
		const cX = xDataToCanvas(this.pointsWithDataSets[0].x, this.state);
		if (!this.model) {
			this.model = this.computeModel(this.pointsWithDataSets);
		}
		this.updateHeight(this.model, this.pointsWithDataSets);
		this.updatePosition([cX, topY], this.model);
		if (this.options.showVerticalLine && !this.state.isBarChart) {
			this.drawLine(cX, 0);
		}
		this.highlightDataSets(this.pointsWithDataSets);
		this.drawBox(this.model);
		this.drawHeader(this.model, this.pointsWithDataSets[0].data[0]);
		this.drawBody(this.model, this.pointsWithDataSets);
		ctx.restore();
	}

	private findTopDataSetPointOnScreen(pointsWithDataSets: PointWithDataSet[]): number {
		let topY = 0;
		if (this.state.config.options.percentage) {
			return topY;
		}
		pointsWithDataSets.forEach(pwd => {
			if (this.state.config.options.stacked) {
				topY += pwd.data[1];
			} else {
				topY = Math.max(pwd.data[1], topY);
			}
		});
		return yDataToCanvas(topY, this.state);
	}

	private drawLine(x: number, yFrom: number): void {
		const { ctx } = this;
		ctx.save();
		ctx.beginPath();
		applyLineStyle(ctx, this.options.line);
		ctx.moveTo(x, yFrom);
		ctx.lineTo(x, this.state.chartBounds.innerBounds.height);
		ctx.stroke();
		ctx.restore();
	}

	private highlightDataSets(pointWithDataSets: PointWithDataSet[]): void {
		const { ctx } = this;
		ctx.save();
		let bottom = 0;
		pointWithDataSets.forEach(point => {
			if (point.dataSet.type === DataSetType.LINE) {
				this.drawPoint(ctx, point);
			} else if (point.dataSet.type === DataSetType.BAR) {
				bottom = this.highlightBar(
					ctx, point,
					this.state.config.options.stacked && bottom
						? bottom
						: this.state.chartBounds.innerBounds.height,
				);
			}
		});
		ctx.restore();
	}

	private drawPoint(ctx: CanvasRenderingContext2D, point: PointWithDataSet): void {
		ctx.fillStyle = this.state.config.options.backgroundColor;
		ctx.beginPath();
		applyLineStyle(ctx, (point.dataSet as LineDataSet).lineStyle);
		const cX = xDataToCanvas(point.x, this.state);
		const cY = yDataToCanvas(point.data[1] * point.dataSet.scaleY, this.state);
		ctx.arc(
			cX, cY,
			this.options.pointRadius,
			0, 2 * Math.PI,
		);
		ctx.fill();
		ctx.stroke();
	}

	private highlightBar(
		ctx: CanvasRenderingContext2D,
		point: PointWithDataSet,
		bottom: number,
	): number {
		const cX = xDataToCanvas(point.x, this.state);
		const height = round(point.data[1] * this.state.scaleY);
		const barWidth = this.state.scaleX;
		applyFillStyle(ctx, point.dataSet.options, true);
		// ctx.fillStyle = lightenDarkenColor(point.dataSet.options.color, 30);
		const cY = bottom - height;
		ctx.fillRect(cX - barWidth * 0.5, cY, barWidth, height);
		return cY;
	}

	private drawBox(model: TooltipModelWithBB): void {
		const { ctx } = this;
		const { bb } = model;
		ctx.save();
		ctx.fillStyle = this.options.boxColor || this.state.config.options.backgroundColor;
		applyLineStyle(ctx, this.options.line, true);
		ctx.translate(bb.left, bb.top);
		if (this.options.boxShadowColor) {
			ctx.shadowBlur = 5;
			ctx.shadowColor = this.options.boxShadowColor;
		}
		ctx.fill(model.boxPath);
		ctx.restore();
	}

	private measureHeader(
		pointsWithDataSets: PointWithDataSet[],
	): Dimensions {
		const header = {} as Dimensions;
		const { ctx } = this;
		ctx.save();
		applyFontStyle(ctx, this.options.headerStyle);
		const text = this.options.formatX(pointsWithDataSets[0].data[0], this.state);
		header.width = Math.ceil(ctx.measureText(text).width + this.options.rowGap);
		header.height = this.options.headerStyle.fontSize + this.options.rowGap;
		ctx.restore();
		return header;
	}

	private drawHeader(
		model: TooltipModelWithBB,
		xData: number,
	): void {
		const { ctx } = this;
		ctx.save();
		applyFontStyle(ctx, this.options.headerStyle);
		ctx.textAlign = 'left';
		ctx.fillText(
			this.options.formatX(xData, this.state),
			model.bb.innerBounds.left, model.bb.innerBounds.top + model.header.height - LINE_GAP,
		);
		ctx.restore();
	}

	private measureBody(
		pointsWithDataSets: PointWithDataSet[],
	): TooltipBodyModel {
		const body: TooltipBodyModel = {
			width: 0,
			rowHeight: this.options.valueStyle.fontSize,
			percentageOffset: 0,
		};
		const { ctx } = this;
		ctx.save();
		const longestName = pointsWithDataSets.reduce((longest, pwd) => {
			return pwd.dataSet.name.length > longest.length ? pwd.dataSet.name : longest;
		}, '');
		applyFontStyle(ctx, this.options.dataSetNameStyle);
		const nameWidth = Math.ceil(ctx.measureText(longestName).width);
		applyFontStyle(ctx, this.options.valueStyle);
		const valueWidth = Math.ceil(ctx.measureText(this.options.formatY(this.getMaxY(), this.state)).width);
		body.width = nameWidth + this.options.rowGap + valueWidth;
		if (this.state.config.options.percentage) {
			applyFontStyle(ctx, this.options.headerStyle);
			const percentWidth = Math.ceil(ctx.measureText('99%').width);
			body.percentageOffset = this.options.rowGap + percentWidth;
			body.width += body.percentageOffset;
		}
		ctx.restore();
		return body;
	}

	private getMaxY(): number {
		return this.state.config.options.percentage ? this.state.initialBounds.dataBounds.top : this.state.dataBounds.top;
	}

	private drawBody(
		model: TooltipModelWithBB,
		pointsWithDataSet: PointWithDataSet[],
	): void {
		const { ctx } = this;
		ctx.save();
		const translateX = model.bb.innerBounds.left;
		let translateY = Math.ceil(model.bb.innerBounds.top + model.header.height);
		ctx.translate(translateX, 0);
		let sum = 0;
		if (this.state.config.options.stacked) {
			sum = pointsWithDataSet.reduce((s, pwd) => s + pwd.data[1], 0);
		}
		pointsWithDataSet.forEach(pwd => {
			ctx.translate(0, translateY);
			this.drawRow(ctx, model, pwd.dataSet.name, pwd.dataSet.getOption('color'), pwd.data[1], sum);
			translateY = model.body.rowHeight + this.options.rowGap;
		});
		if (this.hasSummary) {
			ctx.translate(0, translateY);
			this.drawRow(ctx, model, 'All', this.options.dataSetNameStyle.fontColor || '#000000', sum, sum);
		}
		ctx.restore();
	}

	private drawRow(
		ctx: CanvasRenderingContext2D, model: TooltipModelWithBB,
		label: string, valueColor: string, yValue: number, sum: number,
	): void {
		applyFontStyle(ctx, this.options.dataSetNameStyle);
		ctx.textAlign = 'left';
		ctx.fillText(label, model.body.percentageOffset, model.body.rowHeight);
		applyFontStyle(ctx, this.options.valueStyle);
		ctx.textAlign = 'right';
		if (model.body.percentageOffset) {
			ctx.fillText(`${round(yValue * 100 / sum)}%`, model.body.percentageOffset - this.options.rowGap, model.body.rowHeight);
		}
		ctx.fillStyle = valueColor;
		ctx.fillText(this.options.formatY(yValue, this.state), model.bb.innerBounds.width, model.body.rowHeight);
	}

	/**
	 * Logic:
	 * p - top dataset point, we try to put tooltip in order A->B->C
	 *   A
	 * C p B
	 */
	private computeLeftTopCorner(topPoint: PointTuple, dimension: Dimensions): PointTuple {
		const bb = this.state.chartBounds.innerBounds;
		const y = this.options.distanceFromTop;
		const onTop = topPoint[1] - dimension.height - this.options.distanceFromTop >= 0;
		let x = topPoint[0];
		if (onTop) {
			x = topPoint[0] - dimension.width / 2;
			if (x + dimension.width > bb.width) {
				x = bb.width - dimension.width - BOX_BORDER;
			} else if (x < 0) {
				x = BOX_BORDER;
			}
		} else {
			x += this.options.distanceFromTop;
			if (x + dimension.width > bb.width - BOX_BORDER) {
				x = topPoint[0] - this.options.distanceFromTop - dimension.width;
			}
		}
		return [x, y];
	}

	private updateHeight(model: TooltipModelWithBB, pointsWithDataSets: PointWithDataSet[]): void {
		const rows = pointsWithDataSets.length + Number(this.hasSummary);
		const bodyHeight = (model.body.rowHeight + this.options.rowGap) * rows - this.options.rowGap;

		const newHeight = this.options.padding.top + model.header.height + bodyHeight + this.options.padding.bottom;
		if (newHeight !== model.bb.height) {
			model.bb.bottom = model.bb.top + newHeight;
			model.boxPath = this.createBoxPath(model, newHeight);
		}
	}

	private createBoxPath(model: TooltipModelWithBB, height: number): Path2D {
		const path = new Path2D();
		drawBoxShape(path, {
			left: 0,
			right: model.width,
			top: 0,
			bottom: height,
		}, this.options.tooltipCornerRadius);
		return path;
	}

	private updatePosition(topPoint: PointTuple, model: TooltipModelWithBB): void {
		const p = this.options.position
			? this.options.position(model)
			: this.computeLeftTopCorner(topPoint, model.bb);
		model.bb.set({
			left: p[0],
			right: p[0] + model.width,
			top: p[1],
			bottom: p[1] + model.bb.height,
		});
	}

	private computeModel(pointsWithDataSets: PointWithDataSet[]): TooltipModelWithBB {
		const { padding } = this.options;
		const header = this.measureHeader(pointsWithDataSets);
		const body = this.measureBody(pointsWithDataSets);
		const width = padding.left + Math.max(body.width, header.width) + padding.right;
		const model = {
			header,
			body,
			width,
		} as TooltipModelWithBB;
		model.bb = new BoundsWithPadding({
			left: 0,
			right: width,
			top: 0,
			bottom: 0,
			padding,
		});
		return model;
	}
}
