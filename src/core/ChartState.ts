import { CanvasManager, CanvasName } from './CanvasManager';
import { ChartConfigImpl } from './ChartConfig';

import { chartAnimator } from '../animation/chartAnimator';
import { AnimationState } from '../animation/AnimationState';
import { EventName } from '../events/EventName';
import { BoundingBox, Bounds, HorizontalBounds, VerticalBounds } from '../utils/Bounds';
import { BoundsWithPadding } from '../utils/BoundsWithPadding';
import { ScaleRange } from '../utils/ScaleRange';

interface HorizontalRangeAndBounds {
	dataBounds: BoundingBox;
	dataSize: number;
	deltaX: number;
	xFrom: number;
}

export class ChartState {
	public readonly dataBounds: Bounds;
	public chartBounds!: BoundsWithPadding;
	public chartRect!: ClientRect | DOMRect;
	public readonly ctx: CanvasRenderingContext2D;
	public readonly overlayCtx: CanvasRenderingContext2D;
	public readonly scaleRange: ScaleRange;
	public readonly initialBounds: HorizontalRangeAndBounds;

	public readonly canvasManager: CanvasManager = new CanvasManager(this.config.container);

	public isDisabled: boolean = false;

	private _scaleX: number | undefined;
	private _scaleY: number | undefined;

	constructor(
		public readonly config: ChartConfigImpl,
		public readonly isMaster: boolean,
		public readonly isBarChart: boolean,
	) {
		this.initialBounds = this.computeHRangeAndBounds();
		this.dataBounds = new Bounds({
			left: Infinity,
			right: -Infinity,
			bottom: isBarChart ? 0 : config.getOrDefault(config.options.yAxis.min, Infinity),
			top: config.options.percentage ? 100 : 0,
		});
		/**
		 * bottom > top
		 */
		this.scaleRange = new ScaleRange(this, {
			left: 0,
			right: 1,
			top: 1,
			bottom: 0,
		});

		if (isBarChart) {
			config.container.style.opacity = `${this.config.options.barChartOpacity}`;
		}
		const ctx = config.container.getContext('2d');
		if (!ctx) {
			throw new Error('Can\'t create 2d context');
		}
		this.ctx = ctx;
		this.overlayCtx = this.canvasManager.create(CanvasName.overlay, { zIndex: 2 });
		this.onResize();
	}

	public onResize(): void {
		const chartRect = this.canvasManager.resize();
		this.chartBounds = new BoundsWithPadding({
			left: 0,
			right: chartRect.width,
			bottom: chartRect.height,
			top: 0,
			padding: this.config.options.padding,
		});
		this.chartRect = chartRect;
		this.resetScale();
	}

	public updateBounds(newBounds: VerticalBounds, withAnimation: boolean): void {
		this.resetScale();
		const from = this.dataBounds.toBoundingBox();
		const hBounds = this.calcHBounds();
		this.dataBounds.setHorizontal(hBounds);
		if (newBounds.top === newBounds.bottom) {
			// console.log('invalid bounds', newBounds);
			return;
		}
		const to = Object.assign(newBounds, hBounds);
		this.config.container.dispatchEvent(new CustomEvent(EventName.updateVBounds, {
			detail: { from, to },
		}));
		if (
			this.dataBounds.top === newBounds.top
			&& this.dataBounds.bottom === newBounds.bottom
		) {
			return;
		}
		if (!withAnimation) {
			this.dataBounds.setVertical(newBounds);
			return;
		}
		const animation = new AnimationState(
			this.ctx, this,
			this.config.options.animation.duration.scroll, {
				from, to,
			});
		chartAnimator.add(animation);
	}

	public animateDataBounds(animation: AnimationState | undefined): void {
		if (!animation) {
			return;
		}
		this.dataBounds.setVertical(animation.applyToVBounds(
			animation.from,
			animation.to,
		));
		this.resetScale();
	}

	private calcHBounds(): HorizontalBounds {
		const width = this.initialBounds.dataBounds.right - this.initialBounds.dataBounds.left;
		return {
			left: this.dataBounds.left = this.initialBounds.dataBounds.left + this.scaleRange.left * width,
			right: this.dataBounds.right = this.initialBounds.dataBounds.left + this.scaleRange.right * width,
		};
	}

	get scaleX(): number {
		if (!this._scaleX) {
			this._scaleX = this.chartBounds.innerBounds.width / this.dataBounds.width;
		}
		return this._scaleX;
	}

	get scaleY(): number {
		if (!this._scaleY) {
			this._scaleY = this.chartBounds.innerBounds.height / this.dataBounds.height;
		}
		return this._scaleY;
	}

	private resetScale(): void {
		this._scaleX = undefined;
		this._scaleY = undefined;
	}

	private computeHRangeAndBounds(): HorizontalRangeAndBounds {
		const result = {
			dataBounds: {
				left: 0,
				right: 0,
				top: 0,
				bottom: 0,
			},
			dataSize: 0,
			deltaX: 0,
			xFrom: 0,
		};
		if (!this.config.data.dataSets.length || !this.config.data.dataSets[0].dataReader.size) {
			return result;
		}
		const dataReader = this.config.data.dataSets[0].dataReader;
		const x0 = dataReader.getX(0);
		const x1 = dataReader.getX(1);
		result.deltaX = x1 - x0;
		result.dataBounds.right = dataReader.size - 1;
		result.xFrom = x0;
		result.dataSize = dataReader.size;
		result.dataBounds.top = this.config.data.dataSets.reduce((max, ds) => {
			let maxDsY = 0;
			for (let i = 0; i < ds.dataReader.size; i++) {
				maxDsY = Math.max(maxDsY, ds.dataReader.getY(i));
			}
			return Math.max(max, maxDsY);
		}, 0);
		return result;
	}
}
