import { DataReader } from '../../src/';
import { TelegramColumn } from '../@types/telegram-chart-data';

export class TelegramDataReader implements DataReader {
	constructor(
		private readonly columnX: TelegramColumn,
		private readonly columnY: TelegramColumn,
	) {}

	public get size(): number {
		return this.columnX.length - 1;
	}

	public getX(index: number): number {
		return this.columnX[index + 1] as number;
	}

	public getY(index: number): number {
		return this.columnY[index + 1] as number;
	}
}
