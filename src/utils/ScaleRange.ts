import { BoundingBox, Bounds, HorizontalBounds } from './Bounds';

import { ChartState } from '../core/ChartState';

export class ScaleRange extends Bounds {
	private readonly _indexBounds: HorizontalBounds;

	constructor(
		private readonly state: ChartState,
		initialData: BoundingBox,
	) {
		super(initialData);
		this._indexBounds = {
			left: 0,
			right: state.initialBounds.dataSize,
		};
	}

	get indexBounds(): HorizontalBounds {
		return this._indexBounds;
	}

	public setHorizontal(horizontalBounds: HorizontalBounds): void {
		super.setHorizontal(horizontalBounds);
		this._indexBounds.left = Math.floor(this.state.initialBounds.dataSize * horizontalBounds.left);
		this._indexBounds.right = Math.ceil(this.state.initialBounds.dataSize * horizontalBounds.right);
	}
}
