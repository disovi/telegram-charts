import { BoundingBox, Dimensions } from './Bounds';
import { getFontString } from './ConfigUtils';
import { round } from './Utils';

import { FontStyle } from '../core/ChartConfig';
import { FillStyle, LineStyle } from '../dataset/BaseDataSet';

export function clipArea(ctx: CanvasRenderingContext2D, bounds: BoundingBox): void {
	ctx.beginPath();
	ctx.rect(bounds.left, bounds.top, bounds.right - bounds.left, bounds.bottom - bounds.top);
	ctx.clip();
}

export function clearBounds(ctx: CanvasRenderingContext2D, bounds: BoundingBox, border: number = 0, clearColor?: string): void {
	const rect = {
		x: bounds.left - border,
		y: bounds.top - border,
		w: bounds.right - bounds.left + border * 2,
		h: bounds.bottom - bounds.top + border * 2,
	};
	if (!clearColor) {
		ctx.clearRect(rect.x, rect.y, rect.w, rect.h);
	} else {
		ctx.save();
		ctx.fillStyle = clearColor;
		ctx.fillRect(rect.x, rect.y, rect.w, rect.h);
		ctx.restore();
	}
}

export function setCanvasSize(container: HTMLCanvasElement, size: Dimensions): number {
	const width = Math.floor(size.width);
	const height = Math.floor(size.height);
	const ratio = window.devicePixelRatio;
	container.width = round(width * ratio);
	container.height = round(height * ratio);
	container.style.width = `${width}px`;
	container.style.height = `${height}px`;
	return window.devicePixelRatio;
}

export function applyLineStyle(ctx: CanvasRenderingContext2D, style: LineStyle, skipOpacity: boolean = false): void {
	ctx.lineWidth = style.lineWidth;
	ctx.strokeStyle = style.color;
	if (style.segments) {
		ctx.setLineDash(style.segments);
	}
	if (style.opacity && !skipOpacity) {
		ctx.globalAlpha = style.opacity;
	}
}

export function applyFillStyle(ctx: CanvasRenderingContext2D, style: FillStyle, skipOpacity: boolean = false): void {
	ctx.fillStyle = style.color;
	if (style.opacity && !skipOpacity) {
		ctx.globalAlpha = style.opacity;
	}
}

export function applyFontStyle(ctx: CanvasRenderingContext2D, style: FontStyle, skipOpacity: boolean = false): void {
	const font = getFontString(style);
	if (font) {
		ctx.font = font;
	}
	if (style.fontColor) {
		ctx.fillStyle = style.fontColor;
	}
	if (style.textAlign) {
		ctx.textAlign = style.textAlign;
	}
	if (style.opacity && !skipOpacity) {
		ctx.globalAlpha = style.opacity;
	}
}

const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');

export function createSVGMatrix(): DOMMatrix {
	return svg.createSVGMatrix();
}

export function drawBoxShape(
	path: CanvasRenderingContext2D | Path2D,
	bb: BoundingBox,
	cornerRadius: number | [number, number, number, number],
): void {
	let radii: [number, number, number, number];
	if (!Array.isArray(cornerRadius)) {
		radii = [cornerRadius, cornerRadius, cornerRadius, cornerRadius];
	} else {
		radii = cornerRadius;
	}

	// left top
	path.moveTo(bb.left, bb.top + radii[0]);
	path.quadraticCurveTo(bb.left, bb.top, bb.left + radii[0], bb.top);
	// right top
	path.lineTo(bb.right - radii[1], bb.top);
	path.quadraticCurveTo(bb.right, bb.top, bb.right, bb.top + radii[1]);
	// right bottom
	path.lineTo(bb.right, bb.bottom - radii[2]);
	path.quadraticCurveTo(bb.right, bb.bottom, bb.right - radii[2], bb.bottom);
	// left bottom
	path.lineTo(bb.left + radii[3], bb.bottom);
	path.quadraticCurveTo(bb.left, bb.bottom, bb.left, bb.bottom - radii[3]);
	// return
	path.lineTo(bb.left, bb.top + radii[0]);
}

export function createCanvasOffscreen(dimensions: Dimensions): HTMLCanvasElement {
	const newCanvas = document.createElement('canvas');
	const scale = setCanvasSize(newCanvas, dimensions);
	const ctx = newCanvas.getContext('2d');
	if (!ctx) {
		throw new Error('Can\'t create context');
	}
	ctx.scale(scale, scale);
	newCanvas.style.width = `${dimensions.width}px`;
	newCanvas.style.height = `${dimensions.height}px`;
	return newCanvas;
}
