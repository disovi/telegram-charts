import { DeepPartial } from 'ts-essentials';

import { ChartOptions, FontStyle } from '../core/ChartConfig';
import { CurtainOptions } from '../curtain/ChartCurtain';
import { GridLinesOptions } from '../grid/GridLinesAndLabels';
import { TooltipOptions } from '../tooltip/Tooltip';

export interface ChartTheme extends Pick<ChartOptions, 'backgroundColor'> {
	tooltip: Pick<TooltipOptions, 'headerStyle' | 'boxColor' | 'boxShadowColor' | 'dataSetNameStyle'>;
	xAxis: {
		gridLines: GridLinesOptions,
		ticks: FontStyle,
	};
	yAxis: {
		gridLines: GridLinesOptions,
		ticks: FontStyle,
	};
	masterChart: {
		curtain: CurtainOptions;
	};
}

export enum ThemeName {
	DAY = 'day',
	NIGHT = 'night',
}

const themeByName: Record<ThemeName, DeepPartial<ChartTheme>> = {
	day: {
		backgroundColor: 'white',
		tooltip: {
			boxColor: 'white',
			headerStyle: {
				fontColor: 'black',
			},
			dataSetNameStyle: {
				fontColor: 'black',
			},
		},
		masterChart: {
			curtain: {
				coveredArea: {
					color: '#E2EEF9',
					opacity: 0.6,
				},
				handle: {
					color: '#C0D1E1',
				},
			},
		},
		xAxis: {
			gridLines: {
				color: '#182D3B',
				opacity: 0.1,
			},
			ticks: {
				fontColor: '#8E8E93',
			},
		},
		yAxis: {
			gridLines: {
				color: '#182D3B',
				opacity: 0.1,
			},
			ticks: {
				fontColor: '#8E8E93',
			},
		},
	},
	night: {
		backgroundColor: '#242F3E',
		tooltip: {
			boxColor: '#253241',
			headerStyle: {
				fontColor: 'white',
			},
			dataSetNameStyle: {
				fontColor: 'white',
			},
		},
		masterChart: {
			curtain: {
				coveredArea: {
					color: '#304259',
					opacity: 0.6,
				},
				handle: {
					color: '#56626D',
				},
			},
		},
		xAxis: {
			gridLines: {
				color: '#FFFFFF',
				opacity: 0.1,
			},
			ticks: {
				fontColor: '#A3B1C2',
				opacity: 0.6,
			},
		},
		yAxis: {
			gridLines: {
				color: '#FFFFFF',
				opacity: 0.1,
			},
			ticks: {
				fontColor: '#A3B1C2',
				opacity: 0.6,
			},
		},
	},
};

export function getTheme(name: ThemeName): DeepPartial<ChartTheme> {
	return themeByName[name];
}
