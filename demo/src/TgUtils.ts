import { TelegramDataReader } from './TelegramDataReader';

import { ChartConfig, DataReader, DataSetConfig, DataSetType } from '../../src';
import { TelegramChartData } from '../@types/telegram-chart-data';

export function tgDataToChartConfig(
	tgData: TelegramChartData,
	legendContainer: HTMLElement,
): ChartConfig {
	const dataSets: DataSetConfig[] = [];
	// TODO: should we validate data?
	const columnX = tgData.columns[0];
	let yMin: number | undefined = 0;
	for (let i = 1; i < tgData.columns.length; i++) {
		const columnY = tgData.columns[i];
		const key = columnY[0];
		const dataReader: DataReader = new TelegramDataReader(columnX, columnY);
		// if (dataReader.size > 2000) {
		// console.warn('The size is too large, largestTriangleThreeBuckets will be used to reduce data');
		// dataReader = new RawDataReader(largestTriangleThreeBuckets(dataReader, 2000));
		// }
		const dsConfig: DataSetConfig = {
			name: tgData.names[key],
			type: tgData.types[key],
			dataReader,
			color: tgData.colors[key],
		};
		switch (dsConfig.type) {
			case DataSetType.LINE:
				Object.assign(dsConfig, {
					lineWidth: 2,
				});
				yMin = undefined;
		}
		dataSets.push(dsConfig);
	}
	return {
		data: { dataSets },
		options: {
			yScaled: tgData.y_scaled,
			stacked: tgData.stacked,
			percentage: tgData.percentage,
			masterChart: {
				initialScaleRange: {
					left: 0.8,
					right: 1,
				},
			},
			xAxis: {
				ticks: {
					minDeltaPerTick: 24 * 60 * 60 * 1000,
					minPointsPerTick: 80,
					labelOffset: {
						y: 15,
					},
				},
			},
			yAxis: {
				min: yMin,
				ticks: {
					minPointsPerTick: 50,
					textAlign: 'left',
					labelOffset: {
						x: 2,
					},
				},
			},
			legend: {
				container: legendContainer,
			},
			padding: {
				left: 10,
				right: 0,
				top: 0,
				bottom: 20,
			},
		},
	};
}
