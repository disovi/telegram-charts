import { easingFunctions, EasingEffect } from './easingFunctions';

import { VerticalBounds } from '../utils/Bounds';
import { round } from '../utils/Utils';

// tslint:disable-next-line: no-any
export type AnimationTarget = object;

export interface AnimationOptions {
	duration: {
		dataSetShowHide: number;
		gridAndLabels: number;
		scroll: number;
	};
}

export interface AnimationStateOptions<TState, TData> {
	name?: string;
	from?: TState;
	to?: TState;
	data?: TData;
	onFinish?: () => void;
	onCancel?: () => void;
}

// tslint:disable-next-line: no-any
export class AnimationState<TState = any, TData = any> {
	public readonly contexts: CanvasRenderingContext2D[];
	// each animation should be executed at least once
	public executed: boolean = false;
	private readonly startTime: number = performance.now();
	private readonly stepsCount: number;
	private readonly timePerStep: number;
	private _finishEventWasTriggered: boolean = false;
	constructor(
		context: CanvasRenderingContext2D | CanvasRenderingContext2D[],
		public readonly target: AnimationTarget,
		public readonly duration: number,
		public readonly options: Readonly<AnimationStateOptions<TState, TData>> = {},
	) {
		this.contexts = Array.isArray(context) ? context : [context];
		this.stepsCount = round(this.duration / 1000 * 60); // 60 fps per second
		this.timePerStep = this.duration / this.stepsCount;
	}

	get from(): TState {
		return this.options.from as TState;
	}

	get to(): TState {
		return this.options.to as TState;
	}

	get data(): TData {
		return this.options.data as TData;
	}

	get currentStep(): number {
		const timeSpent = performance.now() - this.startTime;
		if (timeSpent > this.duration) {
			return this.stepsCount;
		}
		return round(timeSpent / this.timePerStep);
	}

	get isFinished(): boolean {
		return performance.now() - this.startTime >= this.duration;
	}

	public get finishEventWasTriggered(): boolean {
		return this._finishEventWasTriggered;
	}

	public finish(): void {
		this._finishEventWasTriggered = true;
		if (this.options.onFinish) {
			this.options.onFinish();
		}
	}

	public cancel(): void {
		if (this.options.onCancel) {
			this.options.onCancel();
		}
	}

	public applyToVBounds(from: VerticalBounds, to: VerticalBounds, easing: EasingEffect = EasingEffect.linear): VerticalBounds {
		return {
			top: round(this.applyToNumber(from.top, to.top, easing)),
			bottom: round(this.applyToNumber(from.bottom, to.bottom, easing)),
		};
	}

	public applyToNumber(from: number, to: number, easing: EasingEffect = EasingEffect.linear): number {
		return from + this.applyEasing(to - from, easing);
	}

	private applyEasing(x: number, easing: EasingEffect): number {
		return x * easingFunctions[easing](this.currentStep / this.stepsCount);
	}
}
