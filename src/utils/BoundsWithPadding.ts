import { BoundingBox, Bounds } from './Bounds';

export class BoundsWithPadding extends Bounds {
	public readonly padding: BoundingBox;
	private _innerBounds: Bounds | undefined;
	constructor(initialData: BoundingBox & { padding: BoundingBox }) {
		super(initialData);
		this.padding = initialData.padding;
	}

	public set(bounds: BoundingBox): void {
		super.set(bounds);
		if (this._innerBounds) {
			this._innerBounds.set(this.getInnerBBox());
		}
	}

	public reset(): void {
		super.reset();
		this._innerBounds = undefined;
	}

	public get innerBounds(): Bounds {
		if (!this._innerBounds) {
			this._innerBounds = new Bounds(this.getInnerBBox());
		}
		return this._innerBounds;
	}

	public getInnerBBox(): BoundingBox {
		return {
			left: this.left + this.padding.left,
			right: this.right - this.padding.right,
			top: this.top + this.padding.top,
			bottom: this.bottom - this.padding.bottom,
		};
	}
}
