import { CurtainHandle, HandleOptions } from './CurtainHandle';
import { CurtainInteraction } from './CurtainInteraction';
import { getRestrictedMovementForBounds } from './CurtainUtils';

import { chartAnimator } from '../animation/chartAnimator';
import { AnimationState } from '../animation/AnimationState';
import { Chart } from '../core/Chart';
import { ChartState } from '../core/ChartState';
import { FillStyle } from '../dataset/BaseDataSet';
import { HorizontalBounds } from '../utils/Bounds';
import { applyFillStyle } from '../utils/CanvasUtils';
import { round } from '../utils/Utils';

export interface CurtainOptions {
	handle: HandleOptions;
	coveredArea: FillStyle;
	minVisiblePoints: number;
}

export class ChartCurtain {
	public readonly leftHandle: CurtainHandle;
	public readonly rightHandle: CurtainHandle;
	public readonly interaction: CurtainInteraction;

	private readonly options: CurtainOptions;
	private _isMoving: boolean = false;
	private minWidthPx: number = 0;

	constructor(
		private readonly state: ChartState,
		private readonly slaveChart: Chart,
	) {
		this.options = state.config.options.masterChart.curtain;
		this.leftHandle = new CurtainHandle(state, true, 0);
		this.rightHandle = new CurtainHandle(state, false, state.chartBounds.innerBounds.width - this.options.handle.width);
		this.interaction = new CurtainInteraction(state, this);
	}

	public setRange(scaleRange: HorizontalBounds): void {
		const leftHandlePx = this.state.chartBounds.innerBounds.width * scaleRange.left;
		this.leftHandle.bounds.setHorizontal({
			left: leftHandlePx,
			right: leftHandlePx + this.options.handle.width,
		});
		const rightHandlePx = this.state.chartBounds.innerBounds.width * scaleRange.right;
		this.rightHandle.bounds.setHorizontal({
			left: rightHandlePx - this.options.handle.width,
			right: rightHandlePx,
		});
		this.minWidthPx = round(this.options.minVisiblePoints * this.state.scaleX);
		this.singleDraw();
	}

	public get isMoving(): boolean {
		return this._isMoving;
	}

	public set isMoving(isMoving: boolean) {
		this._isMoving = isMoving;
	}

	public draw(): void {
		const ctx = this.state.overlayCtx;
		const { innerBounds } = this.state.chartBounds;
		ctx.save();
		applyFillStyle(ctx, this.options.handle);
		ctx.strokeStyle = this.options.handle.color;
		ctx.beginPath();
		ctx.rect(this.leftHandle.bounds.right, 0, this.rightHandle.bounds.left - this.leftHandle.bounds.right, innerBounds.height);
		ctx.stroke();
		applyFillStyle(ctx, this.options.coveredArea);
		// -1 for fill and stroke like behaviour
		ctx.beginPath();
		ctx.rect(-1, -1, this.leftHandle.bounds.right + 1, innerBounds.height + 2);
		ctx.rect(
			this.rightHandle.bounds.left, -1,
			innerBounds.width - this.rightHandle.bounds.left + 2, innerBounds.height + 2,
		);
		ctx.fill();
		ctx.globalAlpha = this.options.handle.opacity || 1;
		this.leftHandle.draw(ctx);
		this.rightHandle.draw(ctx);
		ctx.restore();
	}

	// used inside CurtainInteraction (Intellij doesn't see this)
	public containsPointHorizontal(x: number): boolean {
		return this.leftHandle.bounds.right < x && this.rightHandle.bounds.left > x;
	}

	public move(target: CurtainHandle | ChartCurtain, movementX: number): void {
		let movementResult = 0;
		const { innerBounds } = this.state.chartBounds;
		if (this.leftHandle === target) {
			movementResult = getRestrictedMovementForBounds(
				this.leftHandle.bounds, movementX,
				{ left: 0, right: this.rightHandle.bounds.left - this.minWidthPx },
			);
			if (movementResult) {
				this.leftHandle.bounds.moveX(movementResult);
			}
		} else if (this.rightHandle === target) {
			movementResult = getRestrictedMovementForBounds(
				this.rightHandle.bounds, movementX,
				{ left: this.leftHandle.bounds.right + this.minWidthPx, right: innerBounds.width },
			);
			if (movementResult) {
				this.rightHandle.bounds.moveX(movementResult);
			}
		}
		if (this === target) {
			const { width: handleWidth } = this.options.handle;
			movementResult = getRestrictedMovementForBounds(
				{ left: this.leftHandle.bounds.right, right: this.rightHandle.bounds.left },
				movementX,
				{ left: handleWidth, right: innerBounds.width - handleWidth },
			);
			if (movementResult) {
				this.leftHandle.bounds.moveX(movementResult);
				this.rightHandle.bounds.moveX(movementResult);
			}
		}
		if (!movementResult) {
			return;
		}
		const { width } = this.state.chartBounds.innerBounds;
		this.slaveChart.setRange({
			left: this.leftHandle.bounds.left / width,
			right: this.rightHandle.bounds.right / width,
		});
		this.singleDraw();
	}

	public singleDraw(): void {
		chartAnimator.add(new AnimationState(this.state.overlayCtx, this, 0));
	}
}
