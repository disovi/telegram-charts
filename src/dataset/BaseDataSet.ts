import { DataSetType } from './DataSetType';

import { AnimationState } from '../animation/AnimationState';
import { ChartConfigImpl } from '../core/ChartConfig';
import { ChartState } from '../core/ChartState';
import { AbstractDataReader, DataReader } from '../datareader/DataReader';
import { RangedDataReader } from '../datareader/RangedDataReader';
import { WrappedDataReader } from '../datareader/WrappedDataReader';
import { Bounds, VerticalBounds } from '../utils/Bounds';
import { ScaleRange } from '../utils/ScaleRange';

export type PointTuple = [number, number];

export interface LineStyle extends FillStyle {
	lineWidth: number;
	segments?: number[];
	opacity: number;
}

export interface FillStyle {
	color: string;
	opacity?: number;
}

export interface BaseDataSetConfig {
	name: string;
	type: DataSetType; // default is line
	dataReader: DataReader;
	color: string;
}

export interface DataSetAttributes {
	config: ChartConfigImpl;
}

export abstract class BaseDataSet<T extends BaseDataSetConfig = BaseDataSetConfig> {
	public get scaleY(): number {
		return this._scaleY;
	}

	public set scaleY(scaleY: number) {
		this._scaleY = scaleY;
	}

	public get dataReader(): AbstractDataReader {
		return this._rawDataReader;
	}

	public get name(): string {
		return this._name;
	}

	public get isVisible(): boolean {
		return this._isVisible;
	}

	public readonly dataBounds: Bounds;
	public animation: AnimationState | undefined;
	public shouldBeVisible: boolean = true;
	public state: number[] = [];
	public stateFrom?: ReadonlyArray<number>;
	protected path!: Path2D;
	private _stateTo?: ReadonlyArray<number>;
	private readonly _rawDataReader: AbstractDataReader;
	private readonly _rangedDataReader: RangedDataReader<AbstractDataReader>;
	private _isVisible: boolean = true;
	private readonly _name: string;
	private _scaleY: number = 1;

	protected constructor(
		public readonly type: DataSetType,
		public readonly options: T,
		private readonly defaultOptions: Partial<T>,
		attributes: DataSetAttributes,
	) {
		if (!options.name) {
			throw new Error('DataSet name is required');
		}
		this._name = options.name;
		const rawDataReader = new WrappedDataReader(options.dataReader);
		this.dataBounds = new Bounds({
			left: rawDataReader.getX(0),
			right: rawDataReader.getX(rawDataReader.size - 1),
			top: -Infinity,
			bottom: attributes.config.getOrDefault(attributes.config.options.yAxis.min, Infinity),
		});
		this._rawDataReader = rawDataReader;
		this._rangedDataReader = new RangedDataReader(rawDataReader);
	}

	public draw(
		ctx: CanvasRenderingContext2D,
		state: ChartState,
		animation: AnimationState<number> | undefined,
		transform: DOMMatrix,
	): void {
		if (!this.isVisible && !this.shouldBeVisible) {
			return;
		}
		ctx.save();
		if (animation) {
			ctx.globalAlpha = animation.applyToNumber(
				animation.from * ctx.globalAlpha,
				animation.to * ctx.globalAlpha,
			);
		}
		if (this.scaleY !== 0 && ctx.globalAlpha !== 0) {
			this.drawHelper(ctx, state, transform);
		}
		ctx.restore();
	}

	public cancelAnimation(): void {
		this.animation = undefined;
		this.stateFrom = this.state;
	}

	public finishAnimation(): void {
		this._isVisible = this.shouldBeVisible;
		this.animation = undefined;
		// console.log(this.name, this.isVisible);
		this.stateFrom = this.stateTo;
		if (this.stateTo) {
			this.state = this.stateTo.slice();
		}
		this._stateTo = undefined;
	}

	public updateVBounds(scaleRange?: ScaleRange): VerticalBounds | void {
		if (!this.shouldBeVisible) {
			return;
		}
		const { dataBounds } = this;
		dataBounds.reset();
		let dataReader = this._rawDataReader;
		if (scaleRange) {
			this._rangedDataReader.scale = scaleRange;
			dataReader = this._rangedDataReader;
		}
		for (const y of dataReader) {
			dataBounds.bottom = Math.min(dataBounds.bottom, y);
			dataBounds.top = Math.max(dataBounds.top, y);
		}
		return dataBounds;
	}

	public getOption<K extends keyof T>(option: K): T[K] {
		return this.options[option] || this.defaultOptions[option] as T[K];
	}

	public createAnimation(ctx: CanvasRenderingContext2D[], duration: number): AnimationState {
		const from = this.isVisible ? 1 : 0;
		const to = 1 - from;
		this.animation = new AnimationState(ctx, this, duration, {
			from,
			to,
			onFinish: () => this.finishAnimation(),
		});
		return this.animation;
	}

	public setPath(path: Path2D, state: number[] | undefined): void {
		this.path = path;
		if (state) {
			this.state = state;
		}
	}

	public setAnimationStateTo(state: ReadonlyArray<number> | undefined): void {
		this._stateTo = state;
		this.stateFrom = this.state.slice();
	}

	public get stateTo(): ReadonlyArray<number> | undefined {
		return this._stateTo;
	}

	protected abstract drawHelper(
		ctx: CanvasRenderingContext2D,
		state: ChartState,
		transform: DOMMatrix,
	): void;
}
