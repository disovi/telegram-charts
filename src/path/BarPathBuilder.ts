import { BasePathBuilder } from './LinePathBuilder';

const halfBar = 0.5;

export class BarPathBuilder implements BasePathBuilder {
	private readonly path: Path2D = new Path2D();

	public addPoint(x: number, y: number): void {
		this.path.lineTo(x - halfBar, y);
		this.path.lineTo(x + halfBar, y);
	}

	public beginPath(): void {
		this.path.moveTo(-halfBar, 0);
	}

	public endPath(xL: number): Path2D {
		this.path.lineTo(xL + halfBar, 0);
		return this.path;
	}
}
