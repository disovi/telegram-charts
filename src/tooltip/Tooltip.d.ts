import { TooltipModel } from './TooltipVertical';

import { FontStyle } from '../core/ChartConfig';
import { DataSet } from '../dataset/dataSetFactory';
import { LineStyle, PointTuple } from '../dataset/BaseDataSet';
import { FormatLabel } from '../grid/GridLinesAndLabels';
import { BoundingBox } from '../utils/Bounds';

export interface TooltipOptions {
	line: LineStyle;
	enabled: boolean;
	boxColor?: string;
	boxShadowColor?: string;
	showVerticalLine: boolean;
	pointRadius: number;
	rowGap: number;
	tooltipCornerRadius: number;
	padding: BoundingBox;
	headerStyle: FontStyle;
	dataSetNameStyle: FontStyle;
	distanceFromTop: number;
	formatY: FormatLabel;
	formatX: FormatLabel;
	valueStyle: FontStyle;
	position?: (model: TooltipModel) => PointTuple;
}

export interface Tooltip {
	setDataAndShow(data: PointWithDataSet[]): void;
	hide(): void;
	draw(): void;
}

export interface PointWithDataSet {
	data: PointTuple;
	dataSet: DataSet;
	x: number;
}
