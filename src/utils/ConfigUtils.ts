import { FontStyle } from '../core/ChartConfig';

export function getFontString(fontStyle: FontStyle): string | undefined {
	if (!fontStyle.fontSize || !fontStyle.fontFamily) {
		return;
	}
	const optWithSpaceOrEmptyString = (opt: string | undefined) => opt ? opt + ' ' : '';

	return `${optWithSpaceOrEmptyString(fontStyle.fontStyle)}`
		+ `${optWithSpaceOrEmptyString(fontStyle.fontWeight)}`
		+ `${fontStyle.fontSize}px `
		+ `${fontStyle.fontFamily}`;
}
