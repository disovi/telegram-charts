import { DataReader } from './DataReader';
import { WrappedDataReader } from './WrappedDataReader';

import { ScaleRange } from '../utils/ScaleRange';

export class RangedDataReader<T extends DataReader> extends WrappedDataReader<T> {
	public scale!: ScaleRange;
	constructor(
		dataReader: T,
	) {
		super(dataReader);
	}

	public get size(): number {
		return this.scale.indexBounds.right - this.scale.indexBounds.left;
	}

	public getX(index: number): number {
		return super.getX(index + this.scale.indexBounds.left);
	}

	public getY(index: number): number {
		return super.getY(index + this.scale.indexBounds.left);
	}
}
