export * from './core/Chart';
export * from './dataset/DataSetType';
export * from './dataset/BaseDataSet';
export { DataReader } from './datareader/DataReader';
export { ChartConfig } from './core/ChartConfig';
export * from './theme/ChartTheme';
export * from './utils/Utils';
export { DataSetConfig } from './dataset/dataSetFactory';

// DONE: preserve Y labels positions
// DONE: preserve canvas ctx after each draw (save data to offscreen canvas for tooltip performance)
// DONE: Y-scale on line graphs should start with the lowest visible value
// DONE: A long tap on any data filter should uncheck all other filters
// DONE: New data
// DONE: 2. 2Y axes line chart
// DONE: 3. stacked bar chart
// DONE: 4. bar chart
// DONE: 5. area chart
// DONE: apply colors from design specification
// DONE: add percentage to tooltip
// DONE: format handlers in a new way
// TODO: fix grid labels on scroll
// DONE: update tooltip on visibility change
// TODO: zoom in and out
// TODO: pie chart
