import { DeepPartial } from 'ts-essentials';

export function assignDeeply<T>(to: T, from: DeepPartial<T>): T {
	// @ts-ignore
	// tslint:disable-next-line: no-any
	Object.entries(from).forEach(([key, option]: [keyof T, any]) => {
		if (!Array.isArray(option)
			// tslint:disable-next-line: strict-type-predicates
			&& to[key] !== undefined
			&& option instanceof Object
			&& option.constructor === Object
		) {
			assignDeeply(to[key], option);
		} else {
			to[key] = option;
		}
	});
	return to;
}

// TODO: if precision < 1 we should use toFixed
export function floorToPrecision(x: number, precision: number): number {
	return Math.floor(x - x % precision);
}

export function ceilToPrecision(x: number, precision: number): number {
	return floorToPrecision(x, precision) + precision;
}

export function without<T>(arr1: T[], arr2: T[]): T[] {
	if (!arr2.length) {
		return arr1;
	}
	return arr1.filter(v => !arr2.includes(v));
}

// https://jsperf.com/math-round-vs-hack/53
export function round(x: number): number {
	return ~~(0.5 + x);
}

export const noop = () => {};

export function debounce(cb: () => void, wait: number): () => void {
	let timeout = 0;
	return () => {
		clearTimeout(timeout);
		timeout = window.setTimeout(cb, wait);
	};
}

export function throttle<T>(cb: (...args: T[]) => void, wait: number): (...args: T[]) => void {
	let executionTime = 0;
	return (...args) => {
		const now = performance.now();
		if (now - executionTime > wait) {
			executionTime = now;
			cb(...args);
		}
	};
}
