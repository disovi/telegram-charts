import { DeepPartial } from 'ts-essentials';

import { ChartConfig, ChartConfigImpl } from './ChartConfig';
import { ChartState } from './ChartState';

import { chartAnimator } from '../animation/chartAnimator';
import { AnimationState } from '../animation/AnimationState';
import { DataSet } from '../dataset/dataSetFactory';
import { DataSetType } from '../dataset/DataSetType';
import { EventName } from '../events/EventName';
import { ChartTheme } from '../theme/ChartTheme';
import { VerticalBounds } from '../utils/Bounds';
import { clearBounds, clipArea, createSVGMatrix } from '../utils/CanvasUtils';

export abstract class AbstractChart {
	public get dataSets(): ReadonlyArray<DataSet> {
		return this._dataSets;
	}

	public readonly state: ChartState;
	protected _dataSets!: DataSet[];
	protected readonly config: ChartConfigImpl;
	private readonly transform: DOMMatrix = createSVGMatrix();

	protected constructor(
		container: HTMLCanvasElement,
		config: ChartConfig,
		isMaster: boolean = false,
	) {
		this.config = new ChartConfigImpl(container, config);
		this.state = new ChartState(
			this.config,
			isMaster,
			!!config.data.dataSets.find(ds => ds.type === DataSetType.BAR),
		);
		chartAnimator.register(this.state.ctx, this.draw);
		chartAnimator.register(this.state.overlayCtx, (animations) => this.drawOverlay(animations));
	}

	public resize(): void {
		this.state.onResize();
		this.clip();
		this.translate();
	}

	public applyTheme(theme: DeepPartial<ChartTheme>): void {
		this.config.applyTheme(theme);
		this.singleDraw();
	}

	public updateDataBounds(withAnimation: boolean): boolean {
		if (this.areAllHidden()) {
			if (!this.state.isDisabled) {
				this.config.container.dispatchEvent(new CustomEvent(EventName.disableChart));
				this.state.isDisabled = true;
			}
			return false;
		}
		const vBounds = this.getVBounds();
		if (this.isRestoringFromHidden() && this.state.isDisabled) {
			this.config.container.dispatchEvent(new CustomEvent(EventName.enableChart));
			this.state.isDisabled = false;
		}
		this.state.updateBounds(vBounds, withAnimation);
		return true;
	}

	protected getVBounds(): VerticalBounds {
		const bounds = {
			top: -Infinity,
			bottom: Infinity,
		};
		this._dataSets.forEach(ds => {
			const newBounds = ds.updateVBounds(this.state.scaleRange);
			if (!newBounds) {
				return;
			}
			bounds.top = Math.max(bounds.top, newBounds.top * ds.scaleY);
			bounds.bottom = Math.min(bounds.bottom, newBounds.bottom * ds.scaleY);
		});
		return bounds;
	}

	protected drawOverlay(animations: AnimationState[]): void {
		const ctx = this.state.overlayCtx;
		clearBounds(ctx, {
			left: 0,
			right: this.state.chartBounds.right,
			top: 0,
			bottom: this.state.chartBounds.innerBounds.bottom + 10,
		});
	}

	protected init(): void {
		this.updateDataBounds(false);
		this.state.onResize();
		this.clip();
		this.translate();
	}

	protected singleDraw(): void {
		chartAnimator.add(new AnimationState<undefined>(this.state.ctx, this, 0));
	}

	protected drawDataSets(ctx: CanvasRenderingContext2D, animations: AnimationState[]): void {
		this.transform.a = this.state.scaleX;
		// if (!this.state.isMaster) {
		// 	console.log(this.state.scaleY * this.state.dataBounds.top);
		// }
		this.transform.f = this.state.dataBounds.top * this.state.scaleY;
		this.transform.e =
			-(this.state.initialBounds.dataBounds.right - this.state.initialBounds.dataBounds.left)
			* this.state.scaleX
			* this.state.scaleRange.left;
		for (let i = this._dataSets.length - 1; i >= 0; i--) {
			const dataSet = this._dataSets[i];
			const dsAnimation = animations.find(a => a.target === dataSet);
			this.transform.d = -this.state.scaleY * dataSet.scaleY;
			dataSet.draw(
				ctx, this.state, dsAnimation, this.transform,
			);
		}
	}

	protected isRestoringFromHidden(): boolean {
		return this.dataSets.every(ds => !ds.isVisible);
	}

	protected clip(): void {
		const bb = this.state.chartBounds.innerBounds.toBoundingBox();
		clipArea(this.state.overlayCtx, bb);
		bb.left = 0;
		clipArea(this.state.ctx, bb);
	}

	protected translate(): void {
		const bb = this.state.chartBounds.innerBounds.toBoundingBox();
		if (!this.state.isBarChart) {
			this.state.ctx.lineJoin = 'round';
		}
		this.state.overlayCtx.translate(bb.left, bb.top);
	}

	private areAllHidden(): boolean {
		return this.dataSets.every(ds => !ds.shouldBeVisible);
	}

	private readonly draw = (animations: AnimationState[]): void => {
		this.state.animateDataBounds(animations.find(a => a.target === this.state));
		const ctx = this.state.ctx;
		clearBounds(ctx, this.state.chartBounds, 0);
		if (!this.state.dataBounds.areValid) {
			return;
		}
		ctx.save();
		ctx.translate(this.state.chartBounds.innerBounds.left, this.state.chartBounds.innerBounds.top);
		this.drawDataSets(ctx, animations);
		ctx.restore();
	};
}
