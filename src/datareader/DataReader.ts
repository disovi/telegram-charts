import { PointTuple } from '../dataset/BaseDataSet';

export interface DataReader {
	size: number;
	getY(index: number): number;
	getX(index: number): number;
}

export abstract class AbstractDataReader implements DataReader, Iterable<number> {
	// tslint:disable-next-line:function-name
	public *[Symbol.iterator](): Iterator<number> {
		for (let i = 0; i < this.size; i++) {
			yield this.getY(i);
		}
	}

	public abstract get size(): number;

	public abstract getX(index: number): number;
	public abstract getY(index: number): number;

	public get(index: number): PointTuple {
		return [
			this.getX(index),
			this.getY(index),
		];
	}

	public forEach(cb: (y: number, index: number) => void): void {
		for (let i = 0; i < this.size; i++) {
			cb(this.getY(i), i);
		}
	}
}
