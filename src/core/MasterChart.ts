import { DeepPartial } from 'ts-essentials';

import { AbstractChart } from './AbstractChart';
import { Chart } from './Chart';
import { ChartConfig } from './ChartConfig';

import { AnimationState } from '../animation/AnimationState';
import { ChartCurtain, CurtainOptions } from '../curtain/ChartCurtain';
import { DataSet } from '../dataset/dataSetFactory';
import { ChartTheme } from '../theme/ChartTheme';
import { HorizontalBounds, VerticalBounds } from '../utils/Bounds';
import { drawBoxShape } from '../utils/CanvasUtils';

export interface MasterChartOptions {
	curtain: CurtainOptions;
	initialScaleRange?: HorizontalBounds;
}

interface MasterChartData {
	slaveChart: Chart;
	dataSets: DataSet[];
}

export class MasterChart extends AbstractChart {
	private readonly slaveChart: Chart;
	private readonly curtain: ChartCurtain;

	constructor(container: HTMLCanvasElement, config: ChartConfig, masterChartData: MasterChartData) {
		super(container, config, true);
		this.slaveChart = masterChartData.slaveChart;
		this._dataSets = masterChartData.dataSets;
		this.curtain = new ChartCurtain(this.state, this.slaveChart);
		this.init();
		this.singleDraw();
	}

	public resize(): void {
		super.resize();
		this.curtain.setRange(this.slaveChart.state.scaleRange);
		this.singleDraw();
	}

	public applyTheme(theme: DeepPartial<ChartTheme>): void {
		super.applyTheme(theme);
		this.curtain.singleDraw();
	}

	protected getVBounds(): VerticalBounds {
		if (this.config.options.stacked) {
			return this.slaveChart.pathGenerator.getVBounds(this.state.scaleRange);
		}
		return super.getVBounds();
	}

	protected drawOverlay(animations: AnimationState[]): void {
		super.drawOverlay(animations);
		this.curtain.draw();
	}

	protected init(): void {
		super.init();
		const sr = this.config.options.masterChart.initialScaleRange;
		if (sr) {
			this.curtain.setRange(sr);
		}
	}

	protected clip(): void {
		const path = new Path2D();
		const bb = this.state.chartBounds.innerBounds;
		drawBoxShape(path, bb, this.config.options.masterChart.curtain.handle.cornerRadius);
		this.state.ctx.clip(path);
		this.state.overlayCtx.clip(path);
	}
}
