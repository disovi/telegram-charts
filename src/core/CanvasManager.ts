import { setCanvasSize } from '../utils/CanvasUtils';

interface CanvasOptions {
	zIndex: number;
	alpha?: boolean;
	opacity?: number;
}

export enum CanvasName {
	overlay = 'overlay',
	gridAndLabels = 'grid',
}

export class CanvasManager {
	private readonly canvasByName: Record<string, HTMLCanvasElement> = {};
	constructor(
		private readonly mainCanvas: HTMLCanvasElement,
	) {
	}

	public create(name: string, opts: CanvasOptions): CanvasRenderingContext2D {
		const canvas = document.createElement('canvas');
		canvas.style.zIndex = `${opts.zIndex}`;
		if (opts.opacity) {
			canvas.style.opacity = `${opts.opacity}`;
		}
		canvas.classList.value = this.mainCanvas.classList.value;
		canvas.classList.add(name);
		canvas.width = this.mainCanvas.width;
		canvas.height = this.mainCanvas.height;
		canvas.style.width = this.mainCanvas.style.width;
		canvas.style.height = this.mainCanvas.style.height;
		(this.mainCanvas.parentElement as HTMLDivElement).appendChild(canvas);
		this.canvasByName[name] = canvas;
		return this.getContextAndScale(canvas, opts);
	}

	public get(name: string): HTMLCanvasElement | undefined {
		return this.canvasByName[name];
	}

	public resize(): ClientRect | DOMRect {
		// measure parent element because we fix width and height on our canvas manually
		const chartRect = (this.mainCanvas.parentElement as HTMLElement).getBoundingClientRect();
		setCanvasSize(this.mainCanvas, chartRect);
		Object.values(this.canvasByName).forEach(canvas => {
			setCanvasSize(canvas, chartRect);
			this.getContextAndScale(canvas);
		});
		this.getContextAndScale(this.mainCanvas);
		return this.mainCanvas.getBoundingClientRect();
	}

	private getContextAndScale(canvas: HTMLCanvasElement, contextAttributes?: CanvasRenderingContext2DSettings): CanvasRenderingContext2D {
		const ctx = canvas.getContext('2d', contextAttributes);
		if (!ctx) {
			throw new Error('Can\'t create 2d context');
		}
		ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
		// disable blurry images when using offscreen canvas
		ctx.imageSmoothingEnabled = false;
		return ctx;
	}
}
