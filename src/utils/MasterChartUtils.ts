import { DeepPartial } from 'ts-essentials';

import { assignDeeply } from './Utils';

import { ChartConfig } from '../core/ChartConfig';

const defaultConfig: DeepPartial<ChartConfig> = {
	options: {
		padding: {
			bottom: 0,
			top: 0,
		},
		xAxis: {
			gridLines: {
				display: false,
			},
			ticks: {
				display: false,
			},
		},
		yAxis: {
			gridLines: {
				display: false,
			},
			ticks: {
				display: false,
			},
		},
		tooltip: {
			enabled: false,
		},
		legend: {
			container: undefined,
		},
	},
};

export function buildMasterConfig(config: ChartConfig): ChartConfig {
	assignDeeply(config, defaultConfig);
	return config;
}
