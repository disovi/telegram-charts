import { ChartCurtain } from './ChartCurtain';
import { CurtainHandle } from './CurtainHandle';

import { CanvasName } from '../core/CanvasManager';
import { ChartState } from '../core/ChartState';
import { EventName } from '../events/EventName';
import {
	mouseEventByName,
	normalizeEvent,
	subscribeToTouchEvents,
	DomEventByName,
	DomInteraction, MOUSE_MOVE_THROTTLE, NormalizedEvent,
} from '../utils/DomUtils';
import { mouseEventToChartCanvas } from '../utils/ProjectionUtils';
import { throttle } from '../utils/Utils';

interface MoveState {
	target: CurtainHandle | ChartCurtain;
	previousMouseMoveEvent: NormalizedEvent;
}

export class CurtainInteraction implements DomInteraction {
	public eventByName: DomEventByName = mouseEventByName;
	private moveState: MoveState | undefined;
	private readonly targets: Array<CurtainHandle | ChartCurtain> = [];
	private readonly overlay: HTMLCanvasElement;
	private readonly onMouseMove: (event: Event) => void = throttle((event) => {
		const normalized = normalizeEvent(event, this.state.chartRect);
		// movenmentX/movenmentY aren't supported by safari/ie https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/movementX#Browser_compatibility
		if (this.moveState) {
			this.curtain.move(this.moveState.target, normalized.clientX - this.moveState.previousMouseMoveEvent.clientX);
			this.moveState.previousMouseMoveEvent = normalized;
		}
	}, MOUSE_MOVE_THROTTLE);

	constructor(
		private readonly state: ChartState,
		private readonly curtain: ChartCurtain,
	) {
		this.targets.push(curtain.leftHandle, curtain.rightHandle, curtain);
		this.overlay = this.state.canvasManager.get(CanvasName.overlay) as HTMLCanvasElement;
		this.state.config.container.addEventListener(EventName.disableChart, () => {
			this.removeListeners(this.eventByName);
		});
		this.state.config.container.addEventListener(EventName.enableChart, () => {
			this.addListeners(this.eventByName);
		});
		subscribeToTouchEvents(this, this.overlay);
	}

	public addListeners(eventByName: DomEventByName): void {
		const passive = { passive: true };
		this.overlay.addEventListener(eventByName.down, this.onPointerDown, passive);
		// we bind mouseup and mousemove events to the document to be more interactive
		document.addEventListener(eventByName.up, this.onMouseUp, passive);
	}

	public removeListeners(eventByName: DomEventByName): void {
		this.overlay.removeEventListener(eventByName.down, this.onPointerDown);
		document.removeEventListener(eventByName.up, this.onMouseUp);
	}

	public readonly onPointerDown = (event: Event): void => {
		const normalized = normalizeEvent(event, this.state.chartRect);
		const canvasPoint = mouseEventToChartCanvas(normalized, this.state);
		const target = this.targets.find(t => t.containsPointHorizontal(canvasPoint[0], 5));
		if (target) {
			this.addOnMouseMoveListener({
				previousMouseMoveEvent: normalized,
				target,
			});
		}
	};

	private readonly onMouseUp = (): void => {
		this.removeMouseMoveListener();
	};

	private addOnMouseMoveListener(moveState: MoveState): void {
		this.moveState = moveState;
		this.curtain.isMoving = true;
		document.addEventListener(this.eventByName.move, this.onMouseMove, { passive: true });
	}

	private removeMouseMoveListener(): void {
		this.curtain.isMoving = false;
		document.removeEventListener(this.eventByName.move, this.onMouseMove);
		this.moveState = undefined;
	}
}
