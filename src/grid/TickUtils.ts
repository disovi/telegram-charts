import { AxeDirection, TickOptions } from './GridLinesAndLabels';

import { floorToPrecision, round } from '../utils/Utils';

const DESIRED_LABELS_COUNT = 6;

export function calcDataPerTick(config: TickOptions, pixelSize: number, dataSize: number, direction: AxeDirection): number {
	const pixelsPerTick = Math.ceil(Math.max(config.minPointsPerTick, pixelSize / dataSize));
	let dataPerTick = dataSize / (pixelSize / pixelsPerTick);
	const minDeltaPerTick = direction === AxeDirection.HORIZONTAL ? 1 : getDeltaPerTick(dataSize);
	// const minDeltaPerTick = 1;
	if (dataPerTick < minDeltaPerTick) {
		dataPerTick = minDeltaPerTick;
	} else if (direction === AxeDirection.HORIZONTAL) {
		dataPerTick = Math.pow(2, round(Math.log2(dataPerTick / minDeltaPerTick))) * minDeltaPerTick;
	} else {
		dataPerTick = floorToPrecision(dataPerTick, minDeltaPerTick);
	}
	return dataPerTick;
}

export function getDeltaPerTick(dataSize: number): number {
	const dataPerTick = dataSize / (DESIRED_LABELS_COUNT - 1);
	const dataPerTickStr = `${round(dataPerTick)}`;
	// 10 => 1, 100 => 10 etc
	const dim = dataPerTickStr.length - 1;
	let minDelta = 10 ** dim;
	// 20 => 2, 300 => 30 etc
	const precision = dim > 3 ? 10 ** (dim - floorToPrecision(dim, 3)) : 1;
	minDelta *= round(dataPerTick * precision / minDelta) / precision;
	// console.log(dataSize, minDelta);
	return minDelta;
}
