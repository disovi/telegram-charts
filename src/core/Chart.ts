import { DeepPartial } from 'ts-essentials';

import { AbstractChart } from './AbstractChart';
import { ChartConfig } from './ChartConfig';
import { MasterChart } from './MasterChart';

import { chartAnimator } from '../animation/chartAnimator';
import { AnimationState } from '../animation/AnimationState';
import { createDataSet } from '../dataset/dataSetFactory';
import { EventName } from '../events/EventName';
import { GridLinesAndLabels } from '../grid/GridLinesAndLabels';
import { ChartLegend } from '../legend/ChartLegend';
import { PathGenerator } from '../path/PathGenerator';
import { ChartTheme } from '../theme/ChartTheme';
import { Tooltip } from '../tooltip/Tooltip';
import { TooltipVertical } from '../tooltip/TooltipVertical';
import { HorizontalBounds, VerticalBounds } from '../utils/Bounds';
import { buildMasterConfig } from '../utils/MasterChartUtils';

export class Chart extends AbstractChart {
	public readonly tooltip: Tooltip | undefined;
	public readonly pathGenerator: PathGenerator;
	private readonly gridLinesAndLabels: GridLinesAndLabels;
	private readonly legend: ChartLegend | undefined;
	private readonly masterChart: MasterChart;

	constructor(
		container: HTMLCanvasElement,
		masterChartContainer: HTMLCanvasElement,
		config: ChartConfig,
	) {
		super(container, config);
		const dsAttributes = {
			config: this.config,
		};
		this._dataSets = this.config.data.dataSets.map((dataSetConfig) => createDataSet(dataSetConfig, dsAttributes));
		this.pathGenerator = new PathGenerator(this._dataSets, this.state.initialBounds.dataSize, this.config.options);
		this.pathGenerator.createPaths();
		if (this.config.options.yScaled) {
			this.init2YAxesDataSets();
		}
		this.gridLinesAndLabels = new GridLinesAndLabels(this.state, this);
		this.masterChart = new MasterChart(
			masterChartContainer,
			buildMasterConfig(config),
			{ slaveChart: this, dataSets: this._dataSets },
		);
		if (this.config.options.tooltip.enabled) {
			this.tooltip = new TooltipVertical(this.state, this);
		}
		if (this.config.options.legend.container) {
			this.legend = new ChartLegend(
				this.config.options.legend.container,
				this.config.options.legend,
				this,
			);
		}
		container.addEventListener(EventName.disableChart, () => {
			(container.parentElement as HTMLElement).classList.add('disabled');
		});
		container.addEventListener(EventName.enableChart, () => {
			(container.parentElement as HTMLElement).classList.remove('disabled');
		});
		this.init();
		this.singleDraw();
	}

	public resize(): void {
		super.resize();
		this.gridLinesAndLabels.markDirty();
		this.masterChart.resize();
		this.singleDraw();
	}

	public applyTheme(theme: DeepPartial<ChartTheme>): void {
		this.masterChart.applyTheme(theme);
		this.gridLinesAndLabels.markDirty();
		super.applyTheme(theme);
	}

	public setRange(scale: HorizontalBounds): void {
		this.state.scaleRange.setHorizontal(scale);
		this.updateDataBounds(true);
		this.singleDraw();
	}

	public updateDataSetsVisibility(): void {
		const duration = this.config.options.animation.duration.dataSetShowHide;
		const animationCtx = [this.state.ctx, this.masterChart.state.ctx];
		if (this.config.options.stacked) {
			this.pathGenerator.prepareAnimation();
			const animation = new AnimationState(animationCtx, this.pathGenerator, duration, {
				onCancel: () => this.dataSets.forEach(ds => ds.cancelAnimation()),
				onFinish: () => this.dataSets.forEach(ds => ds.finishAnimation()),
			});
			const boundsTo = this.pathGenerator.getVBoundsTo(this.state.scaleRange);
			if (boundsTo) {
				this.gridLinesAndLabels.updateDataBounds(this.state.dataBounds, boundsTo, true);
				// console.log(boundsTo);
			}
			chartAnimator.add(animation);
		} else {
			this.dataSets.forEach(ds => {
				if (ds.shouldBeVisible !== ds.isVisible) {
					chartAnimator.add(ds.createAnimation(animationCtx, duration));
				}
			});
			this.updateDataBounds(true);
			this.masterChart.updateDataBounds(true);
		}
		if (this.config.options.yScaled) {
			this.gridLinesAndLabels.markDirtyY();
		}
	}

	protected getVBounds(): VerticalBounds {
		if (this.config.options.stacked) {
			if (this.isRestoringFromHidden()) {
				return this.pathGenerator.getVBoundsTo(this.state.scaleRange) || {
					top: 0,
					bottom: 0,
				};
			}
			return this.pathGenerator.getVBounds(this.state.scaleRange);
		}
		return super.getVBounds();
	}

	protected drawOverlay(animations: AnimationState[]): void {
		super.drawOverlay(animations);
		if (this.tooltip) {
			this.tooltip.draw();
		}
	}

	protected drawDataSets(ctx: CanvasRenderingContext2D, animations: AnimationState[]): void {
		if (this.config.options.stacked) {
			const pathAnimation = animations.find(a => a.target === this.pathGenerator);
			if (pathAnimation) {
				this.pathGenerator.animatePaths(pathAnimation);
				this.updateDataBounds(false);
				this.masterChart.updateDataBounds(false);
			}
		}
		super.drawDataSets(ctx, animations);
	}

	protected init(): void {
		const sr = this.config.options.masterChart.initialScaleRange;
		if (sr) {
			this.state.scaleRange.setHorizontal(sr);
		}
		if (this.legend) {
			this.legend.create();
		}
		this.gridLinesAndLabels.markDirty();
		super.init();
	}

	private init2YAxesDataSets(): void {
		if (this._dataSets.length !== 2) {
			throw new Error('Wrong dataSets count');
		}
		this._dataSets.forEach((ds) => {
			ds.updateVBounds(this.state.scaleRange);
		});
		const height0 = this._dataSets[0].dataBounds.height;
		const height1 = this._dataSets[1].dataBounds.height;
		const scaleFirst = height0 < height1;
		if (scaleFirst) {
			this._dataSets[0].scaleY = height1 / height0;
		} else {
			this._dataSets[1].scaleY = height0 / height1;
		}
	}
}
