import { AnimationState, AnimationTarget } from './AnimationState';

type AnimationCallback = (animations: AnimationState[]) => void;

const FPS_INTERVAL = 1000 / 60;

class ChartAnimator {
	private readonly callbackWithAnimationsByCtx: Map<CanvasRenderingContext2D, AnimationCallback> = new Map();
	private readonly 	animations: Set<AnimationState> = new Set();
	private lastAnimation: number = 0;

	public register(ctx: CanvasRenderingContext2D, animateCb: AnimationCallback): void {
		this.callbackWithAnimationsByCtx.set(ctx, animateCb);
	}

	public deregister(ctx: CanvasRenderingContext2D): void {
		this.callbackWithAnimationsByCtx.delete(ctx);
		for (const animation of this.animations) {
			if (animation.contexts.includes(ctx)) {
				this.animations.delete(animation);
			}
		}
	}

	public add(animation: AnimationState): void {
		if (!animation.contexts.every(ctx => this.callbackWithAnimationsByCtx.has(ctx))) {
			throw new Error('Context is not registered');
		}
		const oldAnimation = this.getByTarget(animation.target);
		if (oldAnimation) {
			this.cancel(oldAnimation);
		}
		this.animations.add(animation);
		if (this.animations.size === 1) {
			this.animate();
		}
	}

	public cancel(animation: AnimationState | undefined): void {
		if (animation) {
			animation.cancel();
			this.animations.delete(animation);
		}
	}

	private getByTarget(target: AnimationTarget): AnimationState | undefined {
		for (const animation of this.animations) {
			if (animation.target === target) {
				return animation;
			}
		}
	}

	private readonly animate = (): void => {
		const now = performance.now();
		if (now - this.lastAnimation < FPS_INTERVAL) {
			window.requestAnimationFrame(this.animate);
			return;
		}
		this.lastAnimation = now;
		const animations = [...this.animations];

		if (!animations.length) {
			return;
		}
		animations.forEach(a => {
			if (a.isFinished && !a.finishEventWasTriggered) {
				a.finish();
				// we want animation to be executed once more after finish event
			}
		});
		for (const [ctx, callback] of this.callbackWithAnimationsByCtx.entries()) {
			const animationsForState = animations.filter(a => a.contexts.includes(ctx));
			if (animationsForState.length) {
				callback(animationsForState);
			}
		}
		animations.forEach(a => {
			a.executed = true;
			if (a.isFinished && a.finishEventWasTriggered) {
				this.animations.delete(a);
			}
		});
		if (this.animations.size) {
			window.requestAnimationFrame(this.animate);
		}
	};
}

export const chartAnimator = new ChartAnimator();
