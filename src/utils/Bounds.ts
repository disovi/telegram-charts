import { PointTuple } from '../dataset/BaseDataSet';

export interface HorizontalBounds {
	left: number;
	right: number;
}

export interface VerticalBounds {
	top: number;
	bottom: number;
}

export interface Dimensions {
	width: number;
	height: number;
}

export type BoundingBox = VerticalBounds & HorizontalBounds;

export class Bounds implements BoundingBox, Dimensions {
	public left!: number;
	public right!: number;
	public bottom!: number;
	public top!: number;

	constructor(private readonly initialData: BoundingBox) {
		this.reset();
	}

	get width(): number {
		return this.right - this.left;
	}

	get height(): number {
		return Math.abs(this.top - this.bottom);
	}

	public set(bounds: BoundingBox): void {
		this.left = bounds.left;
		this.right = bounds.right;
		this.top = bounds.top;
		this.bottom = bounds.bottom;
	}

	public setHorizontal(horizontalBounds: HorizontalBounds): void {
		this.left = horizontalBounds.left;
		this.right = horizontalBounds.right;
	}

	public setVertical(verticalBounds: VerticalBounds): void {
		this.top = verticalBounds.top;
		this.bottom = verticalBounds.bottom;
	}

	public get areValid(): boolean {
		return Number.isFinite(this.left) && Number.isFinite(this.top) && this.top !== this.bottom;
	}

	public reset(): void {
		Object.assign(this, this.initialData);
	}

	public toBoundingBox(): BoundingBox & Dimensions {
		return {
			left: this.left,
			right: this.right,
			top: this.top,
			bottom: this.bottom,
			width: this.width,
			height: this.height,
		};
	}

	public containsPoint(p: PointTuple): boolean {
		return p[0] >= this.left && p[0] <= this.right
			&& p[1] >= this.top && p[1] <= this.bottom;
	}

	public containsPointHorizontal(x: number, radius: number = 0): boolean {
		return x >= this.left - radius && x <= this.right + radius;
	}

	public moveX(movementX: number): void {
		this.left += movementX;
		this.right += movementX;
	}

	public equals(b: Bounds | undefined): boolean {
		return b !== undefined
			&& this.left === b.left
			&& this.right === b.right
			&& this.top === b.top
			&& this.bottom === b.bottom;
	}
}
